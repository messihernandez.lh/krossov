<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $portada
 * @property string $title
 * @property string $subtitle
 * @property string $content
 * @property string $images
 * @property boolean $isvideo
 * @property string $urlvideo
 * @property boolean $isurl
 * @property string $url
 * @property string $contentOrden
 * @property string $color
 * @property string $bgcolor
 * @property string $credits
 * @property boolean $isvisible
 * @property boolean $isedit
 * @property string $created_at
 * @property string $updated_at
 * @property Projectgalley[] $projectgalleys
 * @property Projecttext[] $projecttexts
 * @property Projectvideo[] $projectvideos
 */
class ProjectModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'project';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['title','portada','color','bgcolor', 'subtitle', 'content', 'isvideo', 'urlvideo', 'isurl','images', 'url', 'contentOrden', 'credits', 'isvisible','isedit', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectgalleys()
    {
        return $this->hasMany('App\Projectgalley', 'projectid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projecttexts()
    {
        return $this->hasMany('App\Projecttext', 'projectid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectvideos()
    {
        return $this->hasMany('App\Projectvideo', 'projectid');
    }
}
