<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $video
 * @property string $imagen
 * @property string $created_at
 * @property string $updated_at
 */
class Home extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'home';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['video', 'imagen', 'created_at', 'updated_at'];

}
