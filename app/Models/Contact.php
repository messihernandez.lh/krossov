<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $imagen
 * @property string $title
 * @property string $subtitle
 * @property string $oficinas
 * @property string $contacto
 * @property string $created_at
 * @property string $updated_at
 */
class Contact extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'contact';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['imagen', 'title', 'subtitle', 'oficinas', 'contacto', 'created_at', 'updated_at'];

}
