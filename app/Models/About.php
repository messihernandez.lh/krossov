<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $video
 * @property string $imagen
 * @property string $title
 * @property string $subtitle
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 */
class About extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'about';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['video', 'imagen', 'title', 'subtitle', 'content', 'created_at', 'updated_at'];

}
