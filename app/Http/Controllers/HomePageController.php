<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;
use Illuminate\Support\Facades\File;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home=Home::find(1);
        return view("homePage",compact('home'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $home=Home::find($id);
        if ($request->hasFile('video')) {
            $image_path = public_path() . '/assets/home/' . $home->video;
            if (File::exists($image_path) && $home->video != "") {
                unlink($image_path);
            }
            $file = $request->file('video');
            $file->move(public_path() . '/assets/home', $file->getClientOriginalName());
            $home->video = $file->getClientOriginalName();
        }
        if ($request->hasFile('imagen')) {
            $image_path = public_path() . '/assets/home/' . $home->imagen;
            if (File::exists($image_path) && $home->imagen != "") {
                unlink($image_path);
            }
            $file = $request->file('imagen');
            $file->move(public_path() . '/assets/home', $file->getClientOriginalName());
            $home->imagen = $file->getClientOriginalName();
        }

        $home->fondo=$request->fondo;
        $home->texto=$request->texto;
        $home->fondomenu=$request->fondomenu;
        $home->textomenu=$request->textomenu;
        $home->update();
        request()->session()->flash('success', 'Actualizado');
        return redirect(route("home.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
