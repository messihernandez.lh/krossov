<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProjectModel;
use Illuminate\Support\Str;
use App\Mail\sendMail;
use Illuminate\Support\Facades\Mail;
use stdClass;
use App\Models\Home;
use App\Models\About;
use App\Models\Contact;
use App\Models\SocialNetwork;

class FrontendController extends Controller
{
    public $home;
    public $fondo;
    public $texto;
    public $socialNetwork;
    public function __construct()
    {
        $this->home=Home::find(1);
        $this->socialNetwork=SocialNetwork::all();
        $this->fondo=$this->home->fondo;
        $this->texto=$this->home->texto;
    }
    public function index(){
        return view('welcome')->with('home',$this->home)->with('socialNetwork',$this->socialNetwork)->with('fondo',$this->fondo)->with('textcolor',$this->texto);
    }
    public function jobs(){
        return view('jobs.index')->with('home',$this->home)->with('socialNetwork',$this->socialNetwork)->with('fondo',$this->fondo)->with('textcolor',$this->texto);
    }
    public function about(){
        $about=About::find(1);
        return view('about.index',compact('about'))->with('home',$this->home)->with('socialNetwork',$this->socialNetwork)->with('fondo',$this->fondo)->with('textcolor',$this->texto);
    }
    public function contact(){
        $contact=Contact::find(1);
        return view('contact.index',compact('contact'))->with('home',$this->home)->with('socialNetwork',$this->socialNetwork)->with('fondo',$this->fondo)->with('textcolor',$this->texto);
    }
    public function projects()
    {
        $projects = ProjectModel::where('isedit',0)->get();
        // dd($projects);
        return view('projects.index', compact('projects'))->with('home',$this->home)->with('socialNetwork',$this->socialNetwork)->with('fondo',$this->fondo)->with('textcolor',$this->texto);
    }
    public function project($id)
    {
        $project = ProjectModel::where('title', Str::replace('_', ' ', Str::lower($id)))->first();

        $this->fondo=$project->bgcolor;
        $this->texto=$project->color;
        // dd($this->fondo,$this->texto);
        return view('projects.project', compact('project'))->with('home',$this->home)->with('socialNetwork',$this->socialNetwork)->with('fondo',$this->fondo)->with('textcolor',$this->texto);
    }
    public function sendMails(Request $request){
        $datos = new stdClass();
        $datos->from = 'webmaster@krossov.tv';
        $datos->view = 'mails.sendMessage';
        $datos->text = 'mails.sendMessage_txt';
        $datos->fromname = $request->name;
        $datos->txtemail = $request->email;
        $datos->archivo = null;
        $datos->filename = "";
        $datos->nombre = $request->name;
        $datos->tipo = $request->tipo;
        $datos->telefono = $request->tel;
        $datos->correo = $request->email;
        $datos->mensaje = $request->message;

        $datos->subject = 'Mensaje desde la web';
        Mail::to('krossov.tv@gmail.com')->send(new sendMail($datos));
        // Mail::to('oswaldovaldez92@gmail.com')->send(new sendMail($datos));
        return response()->json(['message'=>'send Message','status'=>true],200);
    }
}
