<?php

namespace App\Http\Controllers;

use App\Models\ProjectVideoModel;
use Illuminate\Http\Request;

class ProjectVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($project,Request $request)
    {
        $projectVideo=new ProjectVideoModel();
        $projectVideo->projectid=$request->projectid;
        $projectVideo->projectorden=$request->projectorden;
        $projectVideo->urlvideo="";
        $projectVideo->class="";
        // dd($projectVideo);
        $projectVideo->save();
        return response()->json($projectVideo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectVideoModel  $projectVideoModel
     * @return \Illuminate\Http\Response
     */
    public function show($project,$projectVideoModel)
    {
        return ProjectVideoModel::where('projectorden',$projectVideoModel)->where('projectid',$project)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectVideoModel  $projectVideoModel
     * @return \Illuminate\Http\Response
     */
    public function update($project,Request $request, $projectVideoModel)
    {
		$id=$projectVideoModel;
        $temp=json_decode(json_encode($request->all()));
        $projectVideoModel=ProjectVideoModel::where('projectorden',$request->projectorden)->where('projectid', $request->projectid)
        ->where('id', $request->id)
        ->first();
        //->where('projectid',$project)->first();
        $projectVideoModel->class= $request->class;
        $projectVideoModel->urlvideo= $request->urlvideo;
        $projectVideoModel->update();
        $projectVideoModel->update();
        return response()->json(['message'=>'actualizado']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectVideoModel  $projectVideoModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($project,$projectVideoModel)
    {
        $projectVideoModel=ProjectVideoModel::where('projectorden',$projectVideoModel)->where('projectid',$project)->first();
        // dd($projectVideoModel);
        $projectVideoModel->delete();
    }
}
