<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\About;
use Illuminate\Support\Facades\File;

class AboutPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about=About::find(1);
        return view("aboutPage",compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about=About::find($id);
        if ($request->hasFile('video')) {
            $image_path = public_path() . '/assets/about/' . $about->video;
            if (File::exists($image_path) && $about->video != "") {
                unlink($image_path);
            }
            $file = $request->file('video');
            $file->move(public_path() . '/assets/about', $file->getClientOriginalName());
            $about->video = $file->getClientOriginalName();
        }
        if ($request->hasFile('imagen')) {
            $image_path = public_path() . '/assets/about/' . $about->imagen;
            if (File::exists($image_path) && $about->imagen != "") {
                unlink($image_path);
            }
            $file = $request->file('imagen');
            $file->move(public_path() . '/assets/about', $file->getClientOriginalName());
            $about->imagen = $file->getClientOriginalName();
        }
        $about->title=$request->title;
        $about->subtitle=$request->subtitle;
        $about->content=$this->limpiarHtml($request->content);

        $about->update();
        request()->session()->flash('success', 'Actualizado');
        return redirect(route("about.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function limpiarHtml($texto)
    {
        $_tags = [
            '<!DOCTYPE html>',
            '<html>', '<head>',
            '</head>',
            '<body>',
            '</body>',
            '</html>',
        ];
        // dd($_tags);
        foreach ($_tags as $tag) {
            $texto = str_replace($tag, '', $texto);
        }
        $texto=trim($texto, "\n\r");
        // dd($texto);
        return trim($texto, "\r\n");
    }
}
