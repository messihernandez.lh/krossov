<?php

namespace App\Http\Controllers;

use App\Models\ProjectTextModel;
use Exception;
use Illuminate\Http\Request;

class ProjectTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($project,Request $request)
    {
        $projectTextModel=new ProjectTextModel();
        $projectTextModel->projectid=$request->projectid;
        $projectTextModel->projectorden=$request->projectorden;
        $projectTextModel->bgcolor=$request->bgcolor;
        $projectTextModel->save();
        return response()->json($projectTextModel);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectTextModel  $projectTextModel
     * @return \Illuminate\Http\Response
     */
    public function show($project,$projectTextModel)
    {
        return ProjectTextModel::where('projectorden',$projectTextModel)->where('projectid',$project)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectTextModel  $projectTextModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $projectTextModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectTextModel  $projectTextModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($project,$projectTextModel)
    {

        try{

            $projectTextModel=ProjectTextModel::where('projectorden',$projectTextModel)->where('projectid',$project)->first();
            $projectTextModel->delete();
        }
        catch(Exception $e){
            return response()->json(["message"=>'eliminado']);
        }
        return response()->json(["message" => 'eliminado']);
    }
}
