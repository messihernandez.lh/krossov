<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\File;

class ContactPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact=Contact::find(1);
        return view("contactPage",compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact=Contact::find($id);
        
        $contact->title=$request->title;
        $contact->subtitle=$request->subtitle;
        $contact->oficinas=$this->limpiarHtml($request->oficinas);
        $contact->contacto=$this->limpiarHtml($request->contacto);
        if ($request->hasFile('imagen')) {
            $image_path = public_path() . '/assets/contact/' . $contact->imagen;
            if (File::exists($image_path) && $contact->imagen != "") {
                unlink($image_path);
            }
            $file = $request->file('imagen');
            $file->move(public_path() . '/assets/contact', $file->getClientOriginalName());
            $contact->imagen = $file->getClientOriginalName();
        }
        $contact->update();
        request()->session()->flash('success', 'Actualizado');
        return redirect(route("contact.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function limpiarHtml($texto)
    {
        $_tags = [
            '<!DOCTYPE html>',
            '<html>', '<head>',
            '</head>',
            '<body>',
            '</body>',
            '</html>',
        ];
        // dd($_tags);
        foreach ($_tags as $tag) {
            $texto = str_replace($tag, '', $texto);
        }
        $texto=trim($texto, "\n\r");
        // dd($texto);
        return trim($texto, "\r\n");
    }
}
