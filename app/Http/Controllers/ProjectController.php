<?php

namespace App\Http\Controllers;

use App\Models\ProjectModel;
use App\Models\ProjectTextModel;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use stdClass;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = ProjectModel::paginate(10);
        return view('projects.index', compact('projects'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file());
        $projectModel = new ProjectModel();
        $carpeta = Str::replace(' ', '_', Str::lower($request->title));
        $imagesArr = new stdClass;

        $projectModel->title = $request->title;
        $projectModel->subtitle = $request->subtitle != null ? $request->subtitle : "";
        $projectModel->portada = $request->portada;
        $projectModel->bgcolor = $request->color;
        $projectModel->color = $request->colortext;
        $projectModel->isvisible = $request->has('isvisible');
        $projectModel->isedit = $request->has('isedit');
    
        $projectModel->content = $request->content != null ? trim($request->content) : "";

        $projectModel->isurl = $request->proyecto != null;
        $projectModel->url = $request->proyecto != null ? $request->proyecto : "";
        $projectModel->contentOrden = "";
        $projectModel->credits = $this->limpiarHtml($request->credits,'','');

        $projectModel->isvideo = $request->hasFile('video');
        $projectModel->urlvideo = $request->urlvideo != null ? $request->urlvideo : "";
        
        $image1 = $request->file('image-1');
        if ($image1 != null) {
            $name1 = time() . $image1->getClientOriginalName();
            $image1->move(public_path() . '/assets/projects/' . $carpeta . '/', $name1);
            $indice = 1;
            $imagesArr->$indice = $name1;
        }
        $image2 = $request->file('image-2');
        if ($image2 != null) {
            $name2 = time() . $image2->getClientOriginalName();
            $image2->move(public_path() . '/assets/projects/' . $carpeta . '/', $name2);
            $indice = 2;
            $imagesArr->$indice = $name2;
        }
        $image3 = $request->file('image-3');
        if ($image3 != null) {
            $name3 = time() . $image3->getClientOriginalName();
            
            $image3->move(public_path() . '/assets/projects/' . $carpeta . '/', $name3);
            $indice = 3;
            $imagesArr->$indice = $name3;
        }
        $projectModel->images = json_encode($imagesArr);

        $projectModel->save();
        return redirect(route('project.edit', ['project' => $projectModel->id]))->with('success', 'Proyecto creado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $project = ProjectModel::find($id);
        // dd($project);
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $project = ProjectModel::find($id);
        $project->isedit = $request->has('isedit');
        $imagesArr = json_decode($project->images);
        $carpeta = Str::replace(' ', '_', Str::lower($request->title));
        if($project->title !== $request->title){
            File::copyDirectory(public_path() . '/assets/projects/' . $project->title, public_path() . '/assets/projects/' . $request->title);
            File::deleteDirectory(public_path() . '/assets/projects/' . $project->title);
        }
        $project->title = $request->title;

        $project->subtitle = $request->subtitle != null ? $request->subtitle : "";
        $project->content = $request->content != null ? trim($request->content) : "";
        // $project->images=$request->images;
        $project->isvideo = $request->has('isvideo');
        if($request->has('isvideo')){

            $project->urlvideo = $request->urlvideo != null ? $request->urlvideo : "";
        }
        $project->isurl = $request->has('isurl');
        $project->url = $request->proyecto != null ? $request->proyecto : "";
        // $project->contentOrden=$request->contentOrden;
        $project->color = $request->colortext;
        $project->bgcolor = $request->color;
        $project->credits = $this->limpiarHtml($request->credits,'','');
        $project->isvisible = $request->has('isvisible');
        $image1 = $request->file('image-1');
        if ($image1 != null) {
            $indice = 1;
            $image_path = public_path() . '/assets/projects/'. $carpeta . '/' . $imagesArr->$indice;
            $name = time() . $image1->getClientOriginalName();
            if (File::exists($image_path) && $imagesArr->$indice != "") {
                unlink($image_path);
            }
            $image1->move(public_path() . '/assets/projects/' . $carpeta . '/', $name);
            $imagesArr->$indice = $name;
        }
        $image2 = $request->file('image-2');
        if ($image2 != null) {
            $indice = 2;
            $image_path = public_path() . '/assets/projects/'. $carpeta . '/' . $imagesArr->$indice;
            
            $name = time() . $image2->getClientOriginalName();
            if (File::exists($image_path) && $imagesArr->$indice != "") {
                unlink($image_path);
            }
            $image2->move(public_path() . '/assets/projects/' . $carpeta . '/', $name);
            $imagesArr->$indice = $name;
        }
        $image3 = $request->file('image-3');
        if ($image3 != null) {
            $indice = 3;
            $image_path = public_path() . '/assets/projects/'. $carpeta . '/' . $imagesArr->$indice;
            // dd($image_path);
            if (File::exists($image_path) && $imagesArr->$indice != "") {
                unlink($image_path);
            }
            $name = time() . $image3->getClientOriginalName();
            $image3->move(public_path() . '/assets/projects/' . $carpeta . '/', $name);
            $imagesArr->$indice = $name;
        }
        $project->images = json_encode($imagesArr);
        $project->update();
        if($request->has('section')){

            foreach ($request->section as $section) {
                // dd($section['id']);
                $projecttext = ProjectTextModel::find($section['id']);
                $projecttext->content = $this->limpiarHtml($section['context'],$request->color,$request->colortext);
                $projecttext->update();
            }
        }
        return redirect(route('project.index'));

    }
    public function upgrade(Request $request, $id)
    {
        $project = ProjectModel::find($id);
        $project->contentOrden = $request->contentOrden;
        $project->update();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = ProjectModel::find($id);
        File::deleteDirectory(public_path() . '/assets/projects/' . $project->title);
        $project->delete();
        return redirect(route('project.index'));
    }
    private function limpiarHtml($texto,$fondo,$color)
    {
        $_tags = [
            '<!DOCTYPE html>',
            '<html>', '<head>',
            '</head>',
            '<body>',
            '<style>*{background-color:'.$fondo.';color:'.$color.';}</style>',
            '</body>',
            '</html>',
        ];
        // dd($_tags);
        foreach ($_tags as $tag) {
            $texto = str_replace($tag, '', $texto);
        }
        $texto=trim($texto, "\n\r");
        // dd($texto);
        return trim($texto, "\r\n");
    }
}
