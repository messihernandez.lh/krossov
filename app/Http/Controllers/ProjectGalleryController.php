<?php

namespace App\Http\Controllers;

use App\Models\ProjectGalleryModel;
use Exception;
use Illuminate\Http\Request;
use stdClass;
use Illuminate\Support\Str;

class ProjectGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projectGalleryModel = new ProjectGalleryModel();
        $projectGalleryModel->projectid = $request->projectid;
        $projectGalleryModel->projectorden = $request->projectorden;
        $projectGalleryModel->imgs = "";
        // dd($projectGalleryModel);
        $projectGalleryModel->save();
        return response()->json($projectGalleryModel);

    }

    public function add(Request $request){
        $name="";
        $carpeta = Str::replace(' ', '_', Str::lower($request->title));
        if ($request->type === 'addimage') {

            $image = $request->file('image');
            $name = time() . $image->getClientOriginalName();
                $image->move(public_path() . '/assets/projects/' . $request->carpeta . '/', $name);


        }
        if ($request->type === 'updateimage') {
            $previusName = $request->name;
            $image = $request->file('image');
            $name = time() . $image->getClientOriginalName();
                $image->move(public_path() . '/assets/projects/' . $request->carpeta . '/', $name);
                unlink(public_path() . '/assets/projects/' . $request->carpeta . '/' . $previusName);

        }
        if ($request->type === 'deleteimage') {
            $name = $request->name;
            if($name!="" && $name!="undefined"){
                try{

                    unlink(public_path() . '/assets/projects/' . $request->carpeta . '/' . $name);
                }
                catch(Exception $e){}
            }
            }
        return response()->json(['src' =>  $name]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectGalleryModel  $projectGalleryModel
     * @return \Illuminate\Http\Response
     */
    public function show($project,$projectGalleryModel)
    {
        return ProjectGalleryModel::where('projectorden',$projectGalleryModel)->where('projectid',$project)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectGalleryModel  $projectGalleryModel
     * @return \Illuminate\Http\Response
     */
    public function update($project,Request $request, $projectGalleryModel)
    {
        $project = ProjectGalleryModel::where('projectorden',$projectGalleryModel)->where('projectid',$project)->first();
        $project->imgs = json_encode($request->content);
        $project->update();
    }

    public function upgrade($project,Request $request)
    {

        $name="";
        $carpeta = Str::replace(' ', '_', Str::lower($request->title));
        if ($request->type === 'addimage') {

            $image = $request->file('image');
            $name = time() . $image->getClientOriginalName();
                $image->move(public_path() . '/assets/projects/' . $request->carpeta . '/', $name);


        }
        if ($request->type === 'updateimage') {
            $previusName = $request->name;
            $image = $request->file('image');
            $name = time() . $image->getClientOriginalName();
                $image->move(public_path() . '/assets/projects/' . $request->carpeta . '/', $name);
                unlink(public_path() . '/assets/projects/' . $request->carpeta . '/' . $previusName);

        }
        if ($request->type === 'deleteimage') {
            $name = $request->name;
            if($name!=""){
                unlink(public_path() . '/assets/projects/' . $request->carpeta . '/' . $name);
            }
            }


        return response()->json(['src' =>  $name]);
    }

    /**pro
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectGalleryModel  $projectGalleryModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($project,$projectGalleryModel)
    {
        //
        try {

            $projectGalleryModel = ProjectGalleryModel::where('projectorden', $projectGalleryModel)->where('projectid', $project)->first();
            if (strlen($projectGalleryModel->imgs) > 0) {
                $carpeta = Str::replace(' ', '_', Str::lower($projectGalleryModel->title));
                $objextImgs = json_decode($projectGalleryModel->imgs);
                
                foreach ($objextImgs->img as $img) {
                    try{

                        unlink(public_path() . '/assets/projects/' . $carpeta . '/' . $img->src);
                    } catch (Exception $e) {
                
                    }
                }
            }
            $projectGalleryModel->delete();
        } catch (Exception $e) {
            return response()->json(["message" => 'eliminado']);
        }
        return response()->json(["message" => 'eliminado']);

        
    }
}
