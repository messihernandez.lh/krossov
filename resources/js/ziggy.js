const Ziggy = {"url":"http:\/\/krossov.test","port":null,"defaults":{},"routes":{"project":{"uri":"project\/{id}","methods":["GET","HEAD"]}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export { Ziggy };
