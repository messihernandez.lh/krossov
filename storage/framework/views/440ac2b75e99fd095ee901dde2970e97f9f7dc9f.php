<?php $__env->startSection('content'); ?>
<?php if(intval($project->isvideo)===1): ?>
        
        <?php echo $__env->make('components.modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>
        
<?php echo $__env->make('components.bannerProject.bannerOne', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php if($project->contentOrden!==''): ?>

    <?php $__currentLoopData = json_decode($project->contentOrden); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orden): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php if($orden->type === 'text'): ?>
            <section class="w-full max-w-5xl px-4 container relative z-30 mx-auto mb-20 sm:px-12 <?php if($orden->orden === 1): ?> pt-20 <?php endif; ?>">
                <?php
                    $texto = App\Models\ProjectTextModel::where('projectId', $project->id)
                        ->where('projectOrden', $orden->orden)
                        ->first();
                ?>
                <?php echo $texto->content??''; ?>

            </section>
        <?php endif; ?>
        <?php if($orden->type === 'gallery'): ?>
            <?php
                $gallery = App\Models\ProjectGalleryModel::where('projectId', $project->id)
                    ->where('projectOrden', $orden->orden)
                    ->first();
                $gallery = json_decode($gallery->imgs);

                $typeSlide = $orden->orden+1;
            ?>
            <?php if($gallery!=null): ?>
            <?php echo $__env->make('components.gallery', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('components.galleryMovil', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php endif; ?>

        <?php endif; ?>
        <?php if($orden->type === 'video'): ?>
            <section class="flex items-center justify-center w-full mb-20 md:mx-auto h-screen 2xl:container">
                <?php
                    $video = App\Models\ProjectVideoModel::where('id', $orden->id)
                        ->first();

                ?>
                <v-vimeo :autoplay="<?php echo e(json_encode(false)); ?>" :controls="<?php echo e(json_encode(false)); ?>"
                :loop="<?php echo e(json_encode(true)); ?>"
                :muted="<?php echo e(json_encode(true)); ?>"
                url="<?php echo e($video->urlvideo); ?>"
                videoid="<?php echo e($video->class); ?>"></v-vimeo>


            </section>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    <?php if($project->credits!==''): ?>
        
    <section class="w-full pt-20">
        <?php echo $project->credits; ?>

    </section>
    <?php endif; ?>
    <section class="grid w-full grid-cols-2 pt-20 h-50">
        <div class="relative col-span-1 overflow-hidden">
            <?php
                $identficador = ($project->id);
                $anterior = App\Models\ProjectModel::where('id', '<',$identficador)->where('isedit',0)->orderBy('id', 'desc')->first();

            ?>
            <?php if($anterior!=null): ?>
            <?php
            $images = json_decode($anterior->images, true);
            ?>
            <a
        href="<?php echo e(route('project', ['id' => Str::replace(' ', '_', Str::lower($anterior->title))])); ?>" >
            <img data-src="<?php echo e(asset('assets/projects/' . Str::replace(' ', '_', Str::lower($anterior->title)) . '/' . $images[$anterior->portada===0?1:$anterior->portada])); ?>"
            alt="" class="object-cover object-center w-full h-full lazy">
            <div class="absolute bottom-0 left-0 hidden w-full h-full transition-all duration-300 bg-black bg-opacity-0 lg:block hover:bg-opacity-50 project-hover">
                <h2 class="absolute text-3xl text-gray-100 transition-all duration-150 delay-75 title left-12"><?php echo e($anterior->title); ?></h2>
            </div>
            <div class="absolute bottom-0 left-0 block w-full h-full transition-all duration-300 bg-black bg-opacity-50 lg:hidden">
                <h2 class="absolute text-gray-100 transition-all duration-150 delay-75 transform -translate-x-1/2 translate-y-1/2 bottom-1/2 left-1/2"><?php echo e($anterior->title); ?></h2>
            </div>
        </a>
            <?php endif; ?>
        </div>
        <div class="relative col-span-1 overflow-hidden">
            <?php
            $identficador = ($project->id);
            $siguiente = App\Models\ProjectModel::where('id', '>',$identficador)->where('isedit',0)->first();

        ?>
        <?php if($siguiente!=null): ?>
        <?php
        $images = json_decode($siguiente->images, true);
        ?>
        <a
    href="<?php echo e(route('project', ['id' => Str::replace(' ', '_', Str::lower($siguiente->title))])); ?>" >
        <img data-src="<?php echo e(asset('assets/projects/' . Str::replace(' ', '_', Str::lower($siguiente->title)) . '/' . $images[$siguiente->portada===0?1:$siguiente->portada])); ?>"
        alt="" class="object-cover object-center w-full h-full lazy">
        <div class="absolute bottom-0 left-0 hidden w-full h-full transition-all duration-300 bg-black bg-opacity-0 lg:block hover:bg-opacity-50 project-hover">
            <h2 class="absolute text-3xl text-gray-100 transition-all duration-150 delay-75 title left-12"><?php echo e($siguiente->title); ?></h2>
        </div>
        <div class="absolute bottom-0 left-0 block w-full h-full transition-all duration-300 bg-black bg-opacity-50 lg:hidden">
            <h2 class="absolute text-gray-100 transition-all duration-150 delay-75 transform -translate-x-1/2 translate-y-1/2 bottom-1/2 left-1/2"><?php echo e($siguiente->title); ?></h2>
        </div>
    </a>
        <?php endif; ?>
        </div>

    </section>
    
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script>
        window.addEventListener('load', function () {

            document.querySelectorAll('.video').forEach(function (video) {

                video.querySelector('iframe').classList.add('w-full');
                video.querySelector('iframe').classList.add('h-full');
                video.querySelector('iframe').removeAttribute('height');
                video.querySelector('iframe').removeAttribute('width');
            });
        });
        </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/projects/project.blade.php ENDPATH**/ ?>