<div
class="modal fade"
id="modal-<?php echo e($project->id); ?>"
tabindex="-1"
aria-labelledby="exampleModalLabelType"
aria-hidden="true"
>
<div class="modal-dialog">
  <div class="modal-content dark:bg-gray-800 dark:text-white">
    <div class="modal-header">
      <h5 class="font-semibold modal-title" id="exampleModalLabelType">
        Eliminar
      </h5>
      <button
        type="button"
        class="btn-close dark:text-white"
        data-bs-dismiss="modal"
        aria-label="Close"
      ></button>
    </div>
    <div class="modal-body">
      <div class="flex flex-col w-full gap-3">
       <p>¿Desea eliminar el project <?php echo e($project->title); ?> ?</p>
      </div>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="px-4 py-2 text-sm font-medium  transition focus:outline-none focus:ring-2 focus:ring-offset-2 dark:text-white"
        data-bs-dismiss="modal"
      >
        Cerrar
      </button>
        <form action="<?php echo e(route('project.destroy', ['project'=>$project->id])); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <?php echo method_field('DELETE'); ?>
            <button
            type="submit"
            class="px-4 py-2 text-sm font-medium text-white transition bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
            >
            Eliminar
            </button>
        </form>
      
    </div>
  </div>
</div>
</div><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/admin/views/projects/modal.blade.php ENDPATH**/ ?>