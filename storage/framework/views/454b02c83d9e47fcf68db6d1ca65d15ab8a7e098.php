<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>" data-theme="krossov">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?php echo e(asset('favicon.png')); ?>" />
    <meta name="google-site-verification" content="9uWJnjca7gCtS6tdKkHBX2sS_gP3rh2tn1_Zn-K_3-k" />
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <link rel="stylesheet" href="<?php echo e(asset('themes/krossov/css/app.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('themes/krossov/css/zoom.css')); ?>">

    <title>krossov</title>
<meta name="description" content="Nuestro equipo está en constante evolución, actualizando las nuevas técnicas que se incorporan al mundo de la animación cada año.

    Es importante para nosotros trabajar con los principios de la Gestalt, siendo conscientes de que el diseño es la base de la animación.Equipo Krossov Queremos que nuestro estilo conecte con tu proyecto. Buscamos que nuestras producciones expresen la esencia y personalidad de cada marca y empresa.">
<meta name="keywords" content="contenido audovisual">

<link rel="canonical" href="https://krossov.tv"/>
<meta name="robots" content="1">

    <meta property="og:title" content="krossov.tv" />
<meta property="og:description" content="Nuestro equipo está en constante evolución, actualizando las nuevas técnicas que se incorporan al mundo de la animación cada año.

Es importante para nosotros trabajar con los principios de la Gestalt, siendo conscientes de que el diseño es la base de la animación.Equipo Krossov Queremos que nuestro estilo conecte con tu proyecto. Buscamos que nuestras producciones expresen la esencia y personalidad de cada marca y empresa." />
<meta property="og:site_name" content="krossov.tv" />
<meta property="og:url" content="https://krossov.tv" />
<meta property="og:image" content="https://krossov.tv/favicon.png" />


    <meta name="twitter:creator" content="@krossov" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="krossov.tv" />
<meta name="twitter:site" content="@krossov" />
<meta name="twitter:description" content="Nuestro equipo está en constante evolución, actualizando las nuevas técnicas que se incorporan al mundo de la animación cada año.

Es importante para nosotros trabajar con los principios de la Gestalt, siendo conscientes de que el diseño es la base de la animación.Equipo Krossov Queremos que nuestro estilo conecte con tu proyecto. Buscamos que nuestras producciones expresen la esencia y personalidad de cada marca y empresa." />
<meta name="twitter:url" content="https://krossov.tv/" />
<meta name="twitter:image" content="https://krossov.tv/favicon.png" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@cycjimmy/swiper-animation@4/dist/swiper-animation.umd.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/plugins/animation.gsap.js" integrity="sha512-judXDFLnOTJsUwd55lhbrX3uSoSQSOZR6vNrsll+4ViUFv+XOIr/xaIK96soMj6s5jVszd7I97a0H+WhgFwTEg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?php echo $__env->yieldPushContent('css'); ?>
    <?php if(isset($fondo)): ?>
    <style>
        .fondo-fin{
            background-color:<?php echo e($fondo); ?> !important;
            color:<?php echo e($textcolor); ?> !important;
        }
        body{
            background-color:<?php echo e($fondo); ?> !important;
            color:<?php echo e($textcolor); ?> !important;
        }
        footer{
            background-color:<?php echo e($fondo); ?> !important;
            color:<?php echo e($textcolor); ?> !important;
        }
</style>
<?php endif; ?>
<?php if(isset($home)): ?>
<style>
        .sidebar{
            background-color:<?php echo e($home->fondomenu); ?> !important;
            color:<?php echo e($home->textomenu); ?> !important;
        }
    </style>
    <?php endif; ?>
</head>

<?php if(!isset($fondo) && !isset($textcolor)): ?>

<body class="bg-gray-100 text-black" >
<?php else: ?>

<body class="<?php echo e($fondo!==''?'':'bg-gray-100'); ?> <?php echo e($textcolor!==''?'':'text-black'); ?>" >
<?php endif; ?>
    <div id="barra-menu" class="fixed right-0 w-4 h-screen bg-transparent z-100"></div>
    <div>
        <header>
            <button id="menuburger" class="fixed z-50 bg-base-content menu-burger right-4 top-4">
                <i class="bg-gray-100"></i>
                <i class="bg-gray-100"></i>
                <i class="bg-gray-100"></i>
            </button>
            <div class="gap-4 sidebar z-70">
                <div class="flex flex-row-reverse">
                    <button class="hidden w-12 h-12 transition-all delay-300 bg-base-content close-menu">
                        <i class='text-xl text-gray-100 bx bx-x'></i>
                    </button>

                </div>
                <div class="flex flex-wrap items-center justify-center p-4">
                    <img src="<?php echo e(asset('favicon.png')); ?>" class="w-20" alt="logo">
                </div>
                <ul class="flex flex-col w-full gap-5">
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="<?php echo e(route('index')); ?>">Home</a></li>
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="<?php echo e(route('project.home')); ?>">Work</a></li>
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="<?php echo e(route('project.about')); ?>">About</a></li>
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="<?php echo e(route('project.contact')); ?>">Contact</a></li>
                    
                </ul>
                <ul class="absolute overflow-hidden flex justify-center w-full gap-3 py-4 bottom-2">
                    <?php if(isset($socialNetwork)): ?>
                    <?php $__currentLoopData = $socialNetwork; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $network): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <li>
                        <a href="<?php echo e($network->url); ?>" target="_blank" class="text-xl" rel="nofollow">
                            <?php echo $network->icon; ?>

                        </a>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </div>
        </header>
        <main class="min-h-screen overflow-hidden w-full">
            <div class="" id="app">
                <?php echo $__env->yieldContent('content'); ?>

            </div>
        </main>
        <footer class="relative z-20 w-full max-h-screen py-8">
            <div class="container flex flex-wrap justify-center sm:px-12">
                <div class="flex flex-col lg:flex-row items-center justify-between w-full">
                    <p class="text-lg"><i class='bx bx-copyright'></i> Derechos Reservados <?php echo e(now()->year); ?></p>
                    <div class="flex-grow"></div>
                    <ul class="flex justify-center gap-3 py-4">
                        <?php if(isset($socialNetwork)): ?>
                        <?php $__currentLoopData = $socialNetwork; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $network): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <li>
                        <a href="<?php echo e($network->url); ?>" target="_blank" class="text-xl" rel="nofollow">
                            <?php echo $network->icon; ?>

                        </a>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                        
                    </ul>
                    <div class="flex-grow"></div>
                    <div class="flex items-center gap-3">
                        <p class="text-sm font-bold uppercase">Power by:</p>
	<!--a href="https://redirect.monol4b.com/" target="_blank"-->
                            <img src="<?php echo e(asset('logos/gris.png')); ?>" class="w-36" alt="krossov" />
<!--/a-->
                    </div>
                </div>
                <div class="flex justify-end w-full">
                </div>
            </div>
        </footer>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="<?php echo e(asset('themes/krossov/js/zoom.min.js')); ?>"></script>
    <?php echo $__env->yieldPushContent('script'); ?>
    <script>
        window.addEventListener('load', () => {

            document.querySelector("#menuburger").addEventListener('click', () => {
                document.querySelector('.sidebar').classList.toggle('active');
                window.isEnableMenu = true;
                document.querySelector('.close-menu').classList.remove('hidden');
                document.body.classList.add("overflow-hidden");
            });
            document.querySelector(".close-menu").addEventListener('click', () => {
                document.querySelector('.sidebar').classList.toggle('active');
                document.querySelector('.close-menu').classList.add('hidden');
                window.isEnableMenu = false;
                document.body.classList.remove("overflow-hidden");
            });
        });

    </script>

    <script async src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.4.0/dist/lazyload.min.js"></script>
    <script>
        function logElementEvent(eventName, element) {
            console.log(Date.now(), eventName, element.getAttribute("data-src"));
        }

        var callback_enter = function(element) {
            logElementEvent("🔑 ENTERED", element);
        };
        var callback_exit = function(element) {
            logElementEvent("🚪 EXITED", element);
        };
        var callback_loading = function(element) {
            logElementEvent("⌚ LOADING", element);
        };
        var callback_loaded = function(element) {
            logElementEvent("👍 LOADED", element);
        };
        var callback_error = function(element) {
            logElementEvent("💀 ERROR", element);
            element.src =
                "https://via.placeholder.com/440x560/?text=Error+Placeholder";
        };
        var callback_finish = function() {
            logElementEvent("✔️ FINISHED", document.documentElement);
        };
        var callback_cancel = function(element) {
            logElementEvent("🔥 CANCEL", element);
        };

        window.lazyLoadOptions = {
            threshold: 0,

            callback_enter: callback_enter
            , callback_exit: callback_exit
            , callback_cancel: callback_cancel
            , callback_loading: callback_loading
            , callback_loaded: callback_loaded
            , callback_error: callback_error
            , callback_finish: callback_finish
        };
        window.addEventListener('load', () => {
            let lazyLoadInstance = new LazyLoad(window.lazyLoadOptions);
        });
        window.addEventListener(
            "LazyLoad::Initialized"
            , function(e) {
                console.log(e.detail.instance);
            }
            , false
        );

    </script>
    <script src="<?php echo e(asset('js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('themes/krossov/js/app.js')); ?>" defer></script>


</body>

</html>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/layouts/app.blade.php ENDPATH**/ ?>