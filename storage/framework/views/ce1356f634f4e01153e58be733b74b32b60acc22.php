<?php
$images = json_decode($item->images, true);
?>


<section id="project-<?php echo e($item->id); ?>" class="relative flex flex-nowrap float-left w-screen min-h-screen bg-gray-100 poyectos fondo-fin">
    <div class="absolute top-0 left-0 z-30 h-2 bg-gray-700 bg-opacity-60 progressbar z-60"></div>
    <div class="relative cuadro-screen overflow-hidden gallery-main">
        <div class="absolute z-20 w-full h-full overflow-hidden primero">
            <img src="<?php echo e(asset('assets/projects/' . Str::replace(' ', '_', Str::lower($item->title)) . '/' . $images[1])); ?>" alt="" class="z-20 object-cover object-center w-full h-full max-w-none" />
        </div>
        <div class="absolute z-40 w-full h-full transition-all duration-500 transform segundo">
            <img src="<?php echo e(asset('assets/projects/' . Str::replace(' ', '_', Str::lower($item->title)) . '/' . $images[2])); ?>" alt="" class="z-20 object-cover object-center w-full h-full max-w-none" />
        </div>
        <div class="absolute z-20 w-full h-full overflow-hidden tercero">
            <img src="<?php echo e(asset('assets/projects/' . Str::replace(' ', '_', Str::lower($item->title)) . '/' . $images[3])); ?>" alt="" class="z-20 object-cover object-center w-full h-full max-w-none" />
        </div>
    </div>
    <div class="relative cuadro-rest z-20 flex flex-col items-center justify-center lg:px-20">
        <div class="flex flex-col gap-3 w-full 2xl:w-1/2">
            <h2 class="text-3xl leading-10  uppercase text-opacity-80">
                <?php echo e($item->title); ?>

            </h2>
            <div class="max-w-md">
                <p>
                    <?php echo e($item->content); ?>

                </p>
            </div>
            <a href="<?php echo e(route('project', ['id' => Str::replace(' ', '_', Str::lower($item->title))])); ?>" class="self-start rounded-none btn btn-outline btn-secondary w-max">ver proyecto</a>
        </div>
    </div>
</section>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/components/project.blade.php ENDPATH**/ ?>