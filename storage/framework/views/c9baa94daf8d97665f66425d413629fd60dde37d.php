
<?php if(array_key_exists('gridRows',$gallery->class) && array_key_exists('gridCols',$gallery->class)): ?>
<section class="hidden md:grid  w-full <?php echo e('grid-rows-'.$gallery->class->gridRows ?? '1'); ?>  <?php echo e('md:grid-cols-'.$gallery->class->gridCols ?? '1'); ?> mb-20 section mx-auto min-h-screen">
    <?php $__currentLoopData = $gallery->img; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    
    <?php if(property_exists($img,'src')): ?>

    <div class="<?php echo e($img->classes->columns ?? ''); ?> <?php echo e($img->classes->rows??''); ?>">
        <img class="object-cover object-center w-full h-full lazy" data-src="<?php echo asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . html_entity_decode($img->src)); ?>" data-action="zoom" />
    </div>
    <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</section>
<?php endif; ?>

<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/components/gallery.blade.php ENDPATH**/ ?>