<?php $__env->startSection('content'); ?>
    
    <section>
        <div class="container flex flex-col items-center justify-center py-8 mx-auto">
            <img src="<?php echo e(asset('assets/contatc/'.$contact->imagen)); ?>" class="w-44" alt="logo">
            <div class="flex justify-center w-full gap-5">
                <div class="w-px bg-gray-100 border-white"></div>
                <div class="py-2">
                    <h2 class="text-xl font-semibold md:text-3xl"><?php echo e($contact->title); ?></h2>
                    <p class="text-sm italic md:text-xl"><?php echo e($contact->subtitle); ?></p>
                </div>
            </div>
        </div>
    </section>
    <section class="w-full py-8">
        <div class="container flex flex-wrap justify-between px-4 mx-auto sm:px-12">
            <div class="w-full md:w-1/2 flex flex-col justify-around">
                <div class="flex flex-col items-center sm:px-4 fondo-fin">
                    
                    <?php echo $contact->oficinas; ?>


                </div>
                <div class="flex flex-col items-center sm:px-4 fondo-fin">
                    
                    <?php echo $contact->contacto; ?>

                </div>
            </div>

            <div class="w-full md:w-1/2 flex justify-center">
                <form class="flex flex-col gap-1 max-w-sm md:w-sm" id="formulario-contacto" method="post" action="/api/mail/send" >
                    <h2 class="mb-4 text-3xl text-center">Contactanos</h2>
                    <div class="form-control w-full max-w-sm">
                        <label class="label" for="">Nombre</label>
                        <input class="input w-full max-w-sm rounded-none input-ghost input-bordered input-sm" type="text" name="name" id="">
                    </div>
                    <div class="form-control w-full max-w-sm">
                        <label class="label" for="">Correo</label>
                        <input class="input w-full max-w-sm rounded-none input-ghost input-bordered input-sm" type="email" name="email" id="">
                    </div>
                    <div class="form-control w-full max-w-sm">
                        <label class="label" for="">Telefono</label>
                        <input class="input w-full max-w-sm rounded-none input-ghost input-bordered input-sm" type="text" name="tel" id="">
                    </div>
                    <div class="form-control flex flex-col gap-2 w-full max-w-sm">
                        <label class="flex items-center gap-2">
                            <input type="radio" class="checkbox-secondary checkbox-sm rounded-none checkbox" value="colaboración" name="tipo" id="">
                            <span>Deseo Colaborar</span>
                            </label>
                            <label class="flex items-center gap-2">
                            <input type="radio" class="checkbox-secondary checkbox-sm rounded-none checkbox" value="trabajar" name="tipo" id="">
                            <span>Desea trabajar</span>
                            </label>
                            <label class="flex items-center gap-2">
                                <input type="radio" class="checkbox-secondary checkbox-sm rounded-none checkbox" value="otros" name="tipo" id="">
                                <span>Otro motivo</span>
                                </label>
                    </div>
                    <div class="form-control w-full max-w-sm">
                        <label class="label" for="">Comentario</label>
                        <textarea class="textarea textarea-bordered textarea-ghost rounded-none" name="message" id=""></textarea>
                    </div>
                    <div class="form-control w-full max-w-sm">
                        <button class="btn btn-outline btn-secondary rounded-none w-auto btn-sm" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script>
        window.addEventListener('load',()=>{

            document.querySelector('#formulario-contacto').addEventListener('submit', e => {
                e.preventDefault();
                const form = e.target;
                const data = new FormData(form);
                const url = form.getAttribute('action');
                const method = form.getAttribute('method');
                const headers = new Headers();
                headers.append('X-CSRF-TOKEN', document.querySelector('meta[name="csrf-token"]').getAttribute('content'));
                fetch(url, {
                    method: method,
                    headers: headers,
                    body: data
                }).then(res => res.json()).then(res => {
                    if (res.status) {
                        form.reset();
                        Swal.fire({
                            title: 'Mensaje enviado',
                            text: 'Gracias por contactarnos, pronto nos pondremos en contacto contigo',
                            icon: 'success',
                            confirmButtonText: 'OK',

                        });
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'No se pudo enviar el mensaje, intenta de nuevo',
                            icon: 'error',
                            confirmButtonText: 'OK',

                        });
                    }
                });
            });
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/contact/index.blade.php ENDPATH**/ ?>