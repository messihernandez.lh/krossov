<?php $__env->startSection('content'); ?>
<?php if(session()->has('success')): ?>
<?php echo e(session()->get('success')); ?>

<?php endif; ?>
    <form class="w-full" action="<?php echo e(route('project.update',['project' => $project->id])); ?>" enctype="multipart/form-data" method="post">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <div class="container flex flex-col gap-4 px-4 mx-auto my-8 sm:px-12">
            <div class="flex items-center gap-2 md:w-96">
                <label class="inline-flex items-center mt-3">
                    <input type="checkbox" <?php if($project->isvisible==1): ?> checked <?php endif; ?> class="form-checkbox h-5 w-5 text-gray-600 dark:bg-gray-800 dark:text-white" name="isvisible" id="isvisible" ><span class="ml-2 text-sm font-semibold">Visible en el home</span>
                </label>

                
            </div>
            <div class="flex items-center gap-2 md:w-96">
                <label class="inline-flex items-center mt-3">
                    <input type="checkbox" <?php if($project->isedit==1): ?> checked <?php endif; ?> class="form-checkbox h-5 w-5 text-gray-600 dark:bg-gray-800 dark:text-white" name="isedit" id="isedit" ><span class="ml-2 text-sm font-semibold">Modo Edición</span>
                </label>

                
            </div>
            <div class="flex items-center gap-2 md:w-96">
                <label for="color" class="text-sm font-semibold ">Color de fondo</label>
                
                <input type="color"  name="color" id="color" class="border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" value="<?php echo e($project->bgcolor); ?>">
            </div>
            <div class="flex items-center gap-2 md:w-96">
                <label for="colortext" class="text-sm font-semibold ">Color del texto</label>
                
                <input type="color" name="colortext" id="colortext" class="border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" value="<?php echo e($project->color); ?>">
            </div>
            <div class="flex flex-col gap-2 md:w-96">
                <label for="title" class="text-sm font-semibold " >Titulo</label>
              
                <input type="text" name="title" id="title" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="title" value="<?php echo e($project->title); ?>" required>
            </div>
            <div class="flex flex-col gap-2 md:w-96">
                <label for="subtitle" class="text-sm font-semibold ">Sub Titulo</label>
                <input type="text"  name="subtitle" id="subtitle" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="subtitle" value="<?php echo e($project->subtitle); ?>">
            </div>

            <div class="flex flex-col gap-2 md:w-96">
                <label for="image[]" class="text-sm font-semibold ">Primer Imagen</label>
                <input type="file"  name="image-1" id="image-1" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Imagen 1">
            </div>
            <div class="flex flex-col gap-2 md:w-96">
                <label for="image[1]" class="text-sm font-semibold ">Segunda Imagen</label>
                <input type="file"  name="image-2" id="image-2" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Imagen 2">
            </div>
            <div class="flex flex-col gap-2 md:w-96">
                <label for="image[2]" class="text-sm font-semibold ">Tercer Imagen</label>
                <input type="file"  name="image-3" id="image-2" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Imagen 3">
            </div>
            <div class="flex flex-col gap-2 md:w-96">
                <label for="image[3]" class="text-sm font-semibold ">Imagen de portada</label>
                <select name="portada" id="portada" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm form-select focus:outline-none focus:shadow-outline-blue focus:border-blue-300">
                    <option value="0">Ninguno</option>
                    <option value="1">Primer Imagen</option>
                    <option value="2">Segunda Imagen</option>
                    <option value="3">Tercer Imagen</option>
                </select>
            </div>
            <div class="flex items-center gap-2 md:w-96">
                <label class="inline-flex items-center mt-3">
                    <input type="checkbox" <?php if($project->isvideo==1): ?> checked <?php endif; ?> class="form-checkbox h-5 w-5 text-gray-600 dark:bg-gray-800 dark:text-white" name="isvideo" id="isvideo" ><span class="ml-2 text-sm font-semibold">Video en ventada emergente</span>
                </label>
            </div>
            <div class="flex flex-col items-center gap-2 md:w-96 <?php if($project->isvideo!==1): ?> hidden <?php endif; ?>" id="inputVideo">
                <label for="video" class="text-sm font-semibold ">Video</label>
                    <input type="text"  name="urlvideo" id="urlvideo" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="video" value="<?php echo e($project->urlvideo); ?>">
            </div>
            
            <div class="flex flex-col gap-2 md:w-96">
                <label for="proyecto" class="text-sm font-semibold ">Url del proyecto</label>
                <input type="url" value="<?php echo e($project->url); ?>"  name="proyecto" id="proyecto-3" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="url del proyecto">
            </div>
            <div class="flex flex-col gap-2 md:w-96">
                <label for="content" class="text-sm font-semibold ">Contexto</label>
                <textarea name="content" id="" class="w-full px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm h-72 form-textarea focus:outline-none focus:shadow-outline-blue focus:border-blue-300" row="8"><?php echo e($project->content); ?></textarea>
            </div>
            <div class="flex flex-col gap-2">
                <label for="content" class="text-sm font-semibold ">Creditos</label>
                <textarea name="credit" id="" class="w-full editor px-3 py-2 mt-1 border border-gray-300 dark:bg-gray-800 shadow-sm h-72 form-textarea focus:outline-none focus:shadow-outline-blue focus:border-blue-300" row="8"><?php echo e($project->credit); ?></textarea>
            </div>
        </div>
        <v-project title="<?php echo e(Str::replace(' ', '_', Str::lower($project->title))); ?>":idproject="<?php echo e(json_encode($project->id)); ?>" :iseditable="<?php echo e(json_encode(true)); ?>" :contenido="<?php echo e(json_encode($project->contentOrden)); ?>" ></v-project>
        
        <div class="flex flex-col w-full gap-2 my-4">
            <button class="px-4 py-2 text-sm font-medium text-white transition bg-green-600 w-max hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"type="submit">Actualizar</button>
        </div>
    </form>
     <?php $__env->stopSection(); ?>
<?php $__env->startPush('prescript'); ?>
    <script>
        window.addEventListener('load', () => {
            document.querySelector('#isvideo').addEventListener('change', (e) => {
                if(e.target.checked){
                    document.querySelector('#inputVideo').classList.remove('hidden')
                }else{
                    document.querySelector('#inputVideo').classList.add('hidden')
                }
            })
        });
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/admin/views/projects/edit.blade.php ENDPATH**/ ?>