
<section class="relative w-full mb-20 overflow-hidden md:hidden">
    <div class="w-full" id="slider-<?php echo e($orden->orden); ?>">

        <div class="swiper-wrapper">
            <?php $__currentLoopData = $gallery->img; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(property_exists($img,'src')): ?>
                <div class="swiper-slide">
                    <img class="object-cover object-center w-full h-full lazy"
                        src="<?php echo asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . $img->src); ?>" />
                </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <!-- Add Pagination -->
        <div
            class="absolute z-30 flex justify-center w-full gap-3 mt-8 swiper-pagination-banner-<?php echo e($orden->orden); ?> bottom-4">
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next swiper-button-white text-primary btn-next-<?php echo e($orden->orden); ?>"></div>
        <div class="swiper-button-prev swiper-button-white text-primary btn-prev-<?php echo e($orden->orden); ?>"></div>
    </div>
</section>
<?php switch($typeSlide):
    case (1): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: 'fade',
                            fadeEffect: {
                                crossFade: true,
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php case (2): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "flip",
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php case (3): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: ["-120%", 0, -500],
                                },
                                next: {
                                    shadow: true,
                                    translate: ["120%", 0, -500],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php case (4): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: ["-20%", 0, -1],
                                },
                                next: {
                                    translate: ["100%", 0, 0],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php case (5): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: [0, 0, -800],
                                    rotate: [180, 0, 0],
                                },
                                next: {
                                    shadow: true,
                                    translate: [0, 0, -800],
                                    rotate: [-180, 0, 0],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php case (6): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: ["-125%", 0, -800],
                                    rotate: [0, 0, -90],
                                },
                                next: {
                                    shadow: true,
                                    translate: ["125%", 0, -800],
                                    rotate: [0, 0, 90],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php case (7): ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    origin: "left center",
                                    translate: ["-5%", 0, -200],
                                    rotate: [0, 100, 0],
                                },
                                next: {
                                    origin: "right center",
                                    translate: ["5%", 0, -200],
                                    rotate: [0, -100, 0],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
    <?php break; ?>
    <?php default: ?>
        <?php $__env->startPush('script'); ?>
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-<?php echo e($orden->orden); ?>', {
                            spaceBetween: 0,
                            grabCursor: true,

                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-<?php echo e($orden->orden); ?>',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-<?php echo e($orden->orden); ?>',
                                prevEl: '.btn-prev-<?php echo e($orden->orden); ?>',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-<?php echo e($orden->orden); ?>').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        <?php $__env->stopPush(); ?>
<?php endswitch; ?>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/components/galleryMovil.blade.php ENDPATH**/ ?>