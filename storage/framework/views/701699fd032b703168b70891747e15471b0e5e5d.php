<?php $__env->startSection('content'); ?>
    
    <section>
        <div class="container flex flex-col items-center justify-center py-8 mx-auto">
            <img src="<?php echo e(asset('favicon.png')); ?>" class="w-44" alt="logo">
            <div class="flex justify-center w-full gap-5">
                <div class="w-px bg-gray-100 border-white"></div>
                <div class="py-2">
                    <h2 class="text-xl font-semibold text-gray-800 md:text-3xl">krossov</h2>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="flex flex-wrap justify-center w-full gap-3 min-h-screeen">
        <?php $__currentLoopData = App\Models\ProjectModel::where('isedit',0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="relative overflow-hidden w-72 h-72">

                <?php
                $images = json_decode($project->images, true);
                $portadax= $project->portada;
				if($project->portada===0){
					$portadax= 1;
				}
                ?>
                <a
            href="<?php echo e(route('project', ['id' => Str::replace(' ', '_', Str::lower($project->title))])); ?>" >
                <img data-src="<?php echo asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . $images[$portadax]); ?>"
                alt="" class="object-cover object-center w-full h-full lazy">
                <div class="absolute bottom-0 left-0 hidden w-full h-full transition-all duration-300 bg-black bg-opacity-0 lg:block hover:bg-opacity-50 project-hover">
                    <h2 class="absolute text-3xl text-gray-100 transition-all duration-150 delay-75 title left-12"><?php echo e($project->title); ?></h2>
                </div>
                <div class="absolute bottom-0 left-0 block w-full h-full transition-all duration-300 bg-black bg-opacity-50 lg:hidden">
                    <h2 class="absolute text-gray-100 transition-all duration-150 delay-75 transform -translate-x-1/2 translate-y-1/2 bottom-1/2 left-1/2"><?php echo e($project->title); ?></h2>
                </div>
            </a>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/projects/index.blade.php ENDPATH**/ ?>