<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
  <meta charset="utf-8">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
  <!--[if mso]>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings xmlns:o="urn:schemas-microsoft-com:office:office">
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <style>
    td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-family: "Segoe UI", sans-serif; mso-line-height-rule: exactly;}
  </style>
  <![endif]-->
    <title>krossov mensaje desde la web</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap" rel="stylesheet" media="screen">
    <style>
@media (max-width: 600px) {
  .sm-inline-block {
    display: inline-block !important;
  }
  .sm-hidden {
    display: none !important;
  }
  .sm-leading-32 {
    line-height: 32px !important;
  }
  .sm-p-20 {
    padding: 20px !important;
  }
  .sm-py-12 {
    padding-top: 12px !important;
    padding-bottom: 12px !important;
  }
  .sm-text-center {
    text-align: center !important;
  }
  .sm-text-xs {
    font-size: 12px !important;
  }
  .sm-text-lg {
    font-size: 18px !important;
  }
  .sm-w-1-4 {
    width: 25% !important;
  }
  .sm-w-3-4 {
    width: 75% !important;
  }
  .sm-w-full {
    width: 100% !important;
  }
  .sm-dui17-b-t {
    border: solid #5500ff;
    border-width: 4px 0 0;
  }
}
@media (max-width: 600px) {
  .sm-inline-block {
    display: inline-block !important;
  }
  .sm-w-full {
    width: 100% !important;
  }
  .sm-leading-32 {
    line-height: 32px !important;
  }
}
</style>
</head>
<body style="margin: 0; width: 100%; padding: 0; word-break: break-word; -webkit-font-smoothing: antialiased; background-color: #e5e5e5;">
    <div style="display: none;">krossov®&#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &zwnj;
      &#160;&#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &#847; &zwnj;
      &#160;&#847; &#847; &#847; &#847; &#847; </div>
  <div role="article" aria-roledescription="email" aria-label="Pago Confirmado" lang="en">
    <table width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
          <td align="center" style="padding: 24px;" width="100%">
            <table class="sm-w-full" width="600" cellpadding="0" cellspacing="0" role="presentation">
              <tr>
                <td colspan="2" class="sm-inline-block" style="display: none;">
                  <img src="https://krossov.tv/assets/projects/blend/Blend3015-Styleframe-06.jpg" alt="Double Room" style="max-width: 100%; border: 0; line-height: 100%; vertical-align: middle; border-top-left-radius: 4px; border-top-right-radius: 4px; box-shadow: 0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05);">
                </td>
              </tr>
              <tr>
                <td class="sm-hidden" style="padding-top: 40px; padding-bottom: 40px;" width="160">
                  <img src="https://krossov.tv/assets/projects/busy_bee/SoM_PL_010.png" alt="Double room" style="max-width: 100%; border: 0; line-height: 100%; vertical-align: middle; border-top-left-radius: 4px; border-bottom-left-radius: 4px; box-shadow: 0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05);" width="160">
                </td>
                <td align="left" class="sm-p-20 sm-dui17-b-t" style="border-radius: 2px; padding: 40px; position: relative; box-shadow: 0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05); vertical-align: top; z-index: 50;" bgcolor="#fbfbfb" valign="top">
                  <table width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    <tr>
                      <td width="80%">
                        <h1 class="sm-text-lg" style="font-weight: 700; line-height: 100%; margin: 0; margin-bottom: 4px; font-size: 24px; color: #5500ff;">Mensaje desde la pagina web</h1> </td>
                      <td style="text-align: right;" width="20%" align="right">
                        <img src="https://krossov.tv/logos/logo_1.png" alt="tio ro" style="max-width: 100%; vertical-align: middle; line-height: 100%; border: 0; width: 100%;"> </td>
                    </tr>
                  </table>
                  <div style="line-height: 32px;">&zwnj;</div>
                  <table class="sm-leading-32" style="line-height: 28px; font-size: 14px;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    <tr>
                      <td class="sm-inline-block" style="color: #718096;" width="50%">Nombre</td>
                      <td class="sm-inline-block" style="font-weight: 600; text-align: right;" width="50%" align="right"><?php echo e($datos->nombre); ?></td>
                    </tr>
                    <tr>
                      <td class="sm-inline-block" style="color: #718096;" width="50%">Teléfono</td>
                      <td class="sm-inline-block" style="font-weight: 600; text-align: right;" width="50%" align="right"><?php echo e($datos->telefono); ?></td>
                    </tr>
                    <tr>
                      <td class="sm-w-1-4 sm-inline-block" style="color: #718096;" width="50%">Correo</td>
                      <td class="sm-w-3-4 sm-inline-block" style="font-weight: 600; text-align: right;" width="50%" align="right"><?php echo e($datos->correo); ?></td>
                    </tr>
                    <tr>
                        <td class="sm-w-1-4 sm-inline-block" style="color: #718096;" width="50%">Tipo de mensaje</td>
                        <td class="sm-w-3-4 sm-inline-block" style="font-weight: 600; text-align: right;" width="50%" align="right"><?php echo e($datos->tipo); ?></td>
                      </tr>
                  </table>
                  <table width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    <tr>
                      <td style="padding-top: 24px; padding-bottom: 24px;">
                        <div style="background-color: #edf2f7; height: 2px; line-height: 2px;">&zwnj;</div>
                      </td>
                    </tr>
                  </table>
                  <table style="font-size: 14px;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    <tr>
                      <td class="sm-w-full sm-inline-block sm-text-center" width="40%">
                        <p style="margin: 0; margin-bottom: 4px; color: #a0aec0; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;">Mensaje</p>
                        <p style="font-weight: 600; margin: 0; color: #000000;"><?php echo e($datos->mensaje); ?></p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
  </div>
</body>
</html>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/resources/views/mails/sendMessage.blade.php ENDPATH**/ ?>