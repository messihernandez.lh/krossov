<?php $__env->startSection('content'); ?>
<form class="w-full" action="<?php echo e(route('contact.update',['contact' => $contact->id])); ?>" enctype="multipart/form-data" method="post">
    <?php echo csrf_field(); ?>
    <?php echo method_field('PUT'); ?>
    <div class="container flex flex-col gap-4 px-4 mx-auto my-8 sm:px-12">
        <div class="flex flex-col gap-3 md:w-96">
            <label for="imagen" class="text-sm font-semibold ">Imagen</label>
            <input type="file"  name="imagen" id="video" class="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Imagen">
        </div>
        <div class="flex flex-col gap-3 md:w-96">
            <label for="title" class="text-sm font-semibold ">Titulo</label>
            <input type="text" value="<?php echo e($contact->title); ?>"  name="title" id="title" class="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Titulo">
        </div>
        <div class="flex flex-col gap-3 md:w-96">
            <label for="subtitle" class="text-sm font-semibold ">Sub Titulo</label>
            <input type="text" value="<?php echo e($contact->subtitle); ?>"  name="subtitle" id="subtitle" class="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Sub Titulo">
        </div>
        <div class="flex flex-col gap-3">
            <label for="oficinas" class="text-sm font-semibold ">Direccion de la Oficina</label>
            <textarea class="w-full editor" name="oficinas" >
                <?php echo e($contact->oficinas); ?>

            </textarea>
        </div>
        <div class="flex flex-col gap-3">
            <label for="contacto" class="text-sm font-semibold ">Datos de contacto</label>
            <textarea class="w-full editor" name="contacto" >
                <?php echo e($contact->contacto); ?>

            </textarea>
        </div>
        <div class="flex flex-col flex-col w-full gap-2 my-4">
            <button class="px-4 py-2 text-sm font-medium text-white transition bg-blue-600 w-max hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"type="submit">Actualizar</button>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/admin/views/contactPage.blade.php ENDPATH**/ ?>