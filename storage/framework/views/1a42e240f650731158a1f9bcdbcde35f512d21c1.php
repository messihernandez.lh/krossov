<div class="modal fade" id="modalvideo" tabindex="-1" aria-labelledby="ModalLabelType" aria-hidden="true">
<div class="modal-dialog max-w-4xl 2xl:max-w-7xl 3xl:max-w-screen-2xl modal-dialog-centered">
  <div class="modal-content bg-transparent border-0">

    <button
      type="button"
      data-bs-dismiss="modal"
        class="absolute top-0 right-0 m-4 text-3xl cursor-pointer z-60"
    >
    <i class='bx bx-x text-red-500 text-3xl'></i>
  </button>
    <div class="modal-body border-0 min-h-screen grid">
        <v-vimeo :autoplay="<?php echo e(json_encode(false)); ?>" :controls="<?php echo e(json_encode(false)); ?>"
        :loop="<?php echo e(json_encode(true)); ?>"
        :muted="<?php echo e(json_encode(true)); ?>"
        url="<?php echo e($project->urlvideo); ?>"
        videoid="<?php echo e($project->id); ?>"></v-vimeo>

    </div>
  </div>
</div>
</div>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/components/modal.blade.php ENDPATH**/ ?>