<!DOCTYPE html>

<html class="dark:bg-gray-800 dark:text-white text-gray-800" lang="en">

<head>

    <meta charset="UTF-8">
    <title><?php echo e(config('app.name')); ?> | Admin </title>

    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap"
        rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo e(asset('themes/admin/css/app.css')); ?>">

    <?php echo $__env->yieldPushContent('css'); ?>

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">



</head>

<body class="relative w-full min-h-screen body dark:bg-gray-800 dark:text-white text-gray-800">
  <?php if(!Auth::check()): ?>
      <script>
        window.location = "<?php echo e(route('login')); ?>";
        </script>
    <?php endif; ?>
    <div class="overflow-hidden sidebar">
        <div class="logo_content flex justify-between items-center">
            <div class="logo">
                
                <div class="logo_name dark:text-white text-gray-800"><?php echo e(config('app.name')); ?></div>
            </div>
            <i class='bx bx-menu dark:text-white text-gray-800 cursor-pointer text-2xl' id="btn"></i>
        </div>


        <div class="flex-grow px-4 py-8 grid grid-rows-1">
            <ul class="flex flex-col gap-4">
                <li><a class="text-gray-800 dark:text-white" href="<?php echo e(route('project.index')); ?>">Proyectos</a></li>
                <li><a class="text-gray-800 dark:text-white" href="<?php echo e(route('home.index')); ?>">Pagina Home</a></li>
                <li><a class="text-gray-800 dark:text-white" href="<?php echo e(route('about.index')); ?>">Pagina About</a></li>
                <li><a class="text-gray-800 dark:text-white" href="<?php echo e(route('contact.index')); ?>">Pagina Contact</a></li>
            </ul>
        </div>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <!--<img src="profile.jpg" alt="">-->
                    <div class="flex gap-3 justify-between name_job">
                    <?php if(Auth::check()): ?>
                    <div class="name"><?php echo e(Auth::user()->name); ?></div>
                        <form method="post" action="<?php echo e(route('logout')); ?>">
                            <?php echo csrf_field(); ?>
                            <button type="submit" class="btn btn-square btn-ghost">
                                <i class='bx bx-log-out' id="log_out"></i>
                            </button>
                        </form>
                        
                    </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
    <div class="home_content min-h-screen dark:bg-gray-800 dark:text-white text-gray-800" id="app">
        <div class="relative w-full h-12"></div>
        <div class="fixed top-0 z-40 w-full bg-white dark:bg-gray-800 dark:text-white text-gray-800">
            <div class="container flex justify-between w-full px-4 mx-auto sx:px-12">
                <button
                    class="flex items-center justify-center w-12 h-12 transition-all duration-500 cursor-pointer focus:outline-none"
                    id="btn-outer">

                    <i class='text-2xl bx bx-menu cursos-pointer dark:text-white text-gray-800'></i>
                </button>
                <div class="flex items-center gap-5">
                    <?php if(isset($back)): ?>
                        <a href="<?php echo e($back); ?>" class="text-red-400 hover:text-red-600"><i
                                class="fas fa-arrow-left"></i>
                            Regresar </a>
                    <?php endif; ?>
                    <?php if(isset($title)): ?>
                        <p class="hidden text-sm font-semibold uppercase dark:text-white text-gray-800 lg:inline-block text-shadow">
                            <?php echo e($title); ?></p>
                    <?php endif; ?>
                </div>
                <i class='flex items-center justify-center text-2xl w-12 h-12 transition-all duration-200 ease-in cursor-pointer bx bx-moon dark:text-white text-gray-800'
                    id="btn-dark"></i>

            </div>
        </div>
        <?php echo $__env->yieldContent('content'); ?>



    </div>

    <?php echo $__env->yieldPushContent('prescript'); ?>
    <script src="<?php echo e(asset('js/bootstrap.bundle.min.js')); ?>"></script>
    <script src = "<?php echo e(asset('/js/tinymce/tinymce.js')); ?>" ></script>


    <script src="<?php echo e(asset('themes/admin/js/app.js')); ?>"></script>

    <script>
        let btnDark = document.querySelector("#btn-dark");

        if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia(
                '(prefers-color-scheme: dark)').matches)) {
            document.querySelector('html').classList.add('dark');
            btnDark.classList.replace("bx-moon", "bx-sun");
        } else {
            document.querySelector('html').classList.remove('dark');
            btnDark.classList.replace("bx-sun", "bx-moon");

        }






        let btn = document.querySelector("#btn");
        let btnOuter = document.querySelector("#btn-outer");
        let sidebar = document.querySelector(".sidebar");
        /*let searchBtn = document.querySelector(".bx-search");*/
        document.querySelectorAll('.btn-qr').forEach(btn => {
            btn.addEventListener('click', function() {

                document.querySelector('.qr').classList.toggle('-left-full');
            });
        });
        btnOuter.onclick = () => {
            sidebar.classList.toggle("active");
            if (btn.classList.contains("bx-menu")) {
                btn.classList.replace("bx-menu", "bx-menu-alt-right");
                btnOuter.classList.toggle('opacity-0');
            } else {
                btn.classList.replace("bx-menu-alt-right", "bx-menu");
            }
        };
        btn.onclick = function() {
            sidebar.classList.toggle("active");
            if (btn.classList.contains("bx-menu")) {
                btn.classList.replace("bx-menu", "bx-menu-alt-right");
            } else {
                btn.classList.replace("bx-menu-alt-right", "bx-menu");
                btnOuter.classList.toggle('opacity-0');
            }
        };

        btnDark.onclick = function() {
            document.querySelector('html').classList.toggle('dark');
            if (btnDark.classList.contains("bx-moon")) {
                btnDark.classList.replace("bx-moon", "bx-sun");
                localStorage.theme = 'dark';
            } else {
                btnDark.classList.replace("bx-sun", "bx-moon");
                localStorage.theme = 'light';
            }

        };
        /*  searchBtn.onclick = function() {
            sidebar.classList.toggle("active");
        };  */
    </script>
    <?php echo $__env->yieldPushContent('script'); ?>
    <script>
        let editorConfig = {
            path_absolute: "/",
            selector: `.editor`,
            height: "500px",
            language: "es",
            content_style: `@import  url('https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital,wght@0,400;0,700;1,400&family=Source+Code+Pro:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,900&display=swap');body {font-family: 'Libre Baskerville', monospace;}h1,h2,h3,h4,h5,h6 {font-family: 'Source Code Pro', serif;}`,
            font_formats:
              "Libre Baskerville=Libre Baskerville;Source Code Pro=Source Code Pro;Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Raleway=raleway; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats;",
            plugins: [
              "print preview fullpage paste importcss searchreplace",
              "autolink autosave save directionality code visualblocks",
              "visualchars fullscreen image link media template codesample",
              "table charmap hr pagebreak nonbreaking anchor toc insertdatetime",
              "advlist lists wordcount imagetools textpattern noneditable help",
              "charmap quickbars emoticons spellchecker",
            ],
            menubar: "file edit view insert format tools table help",
            toolbar:
              "undo redo  bold italic underline strikethrough  fontselect fontsizeselect formatselect alignleft aligncenter alignright alignjustify outdent indent numlist bullist  forecolor backcolor removeformat pagebreak charmap emoticons fullscreen preview save print insertfile image media template link anchor codesample fullpage ltr rtl styleselect",
            importcss_append: true,
            relative_urls: false,
            file_picker_callback: function (callback, value, meta) {
              var x =
                window.innerWidth ||
                document.documentElement.clientWidth ||
                document.getElementsByTagName("body")[0].clientWidth;
              var y =
                window.innerHeight ||
                document.documentElement.clientHeight ||
                document.getElementsByTagName("body")[0].clientHeight;

              var cmsURL =
                editorConfig.path_absolute + "filemanager?editor=" + meta.fieldname;
              if (meta.filetype == "image") {
                cmsURL = cmsURL + "&type=Images";
              } else {
                cmsURL = cmsURL + "&type=Files";
              }
              tinyMCE.activeEditor.windowManager.openUrl({
                url: cmsURL,
                title: "Filemanager",
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no",
                onMessage: (api, message) => {
                  callback(message.content);
                },
              });
            },
          };

          tinymce.init(editorConfig);
    </script>

</body>

</html>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/admin/views/layouts/app.blade.php ENDPATH**/ ?>