<?php
$images = json_decode($project->images, true);
$portadax= $project->portada;
if($project->portada===0){
	$portadax= 1;
	}
$image=asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' .
$images[$portadax]);

?>

<section class="relative w-full h-50">
    <img data-src="<?php echo asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . $images[$portadax]); ?>"
        alt="" class="object-cover object-center w-full h-full lazy">
    
    <div class="absolute z-20 flex flex-col w-auto gap-4 bottom-4 left-4 flex-nowrap">
        <div class="w-full px-4 py-2 bg-gradient-to-t from-transparent to-gray-900">
            <h2 class="text-xl text-gray-100 uppercase text-shadow-sm">
                <?php echo e($project->title); ?>

            </h2>

        </div>
        <?php if(intval($project->isvideo)===1): ?>
        <button data-bs-toggle="modal" data-bs-target="#modalvideo"
            class="self-start mb-2 rounded-none btn btn-outline btn-secondary w-max">ver video</button>

        <?php endif; ?>
         <?php if($project->url!=="" && $project->url!==null): ?>
        <a href="<?php echo e($project->url); ?>" target="_blank"
            class="self-start mb-2 rounded-none btn btn-outline btn-secondary w-max">ir al proyecto</a>

        <?php endif; ?>
    </div>
</section>
<?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/components/bannerProject/bannerOne.blade.php ENDPATH**/ ?>