<?php $__env->startSection('content'); ?>
<?php echo $__env->make('components.banner', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<section>
    <div class="container flex flex-col items-center justify-center py-8 mx-auto">
        <img src="<?php echo e(asset('assets/about/'.$about->imagen)); ?>" class="w-44" alt="logo">
        <div class="flex justify-center w-full gap-5">
            <div class="w-px bg-gray-100 fondo-fin border-white"></div>
            <div class="py-2">
                <h2 class="text-xl font-semibold md:text-3xl"><?php echo e($about->title); ?></h2>
                <p class="text-sm italic md:text-xl"><?php echo e($about->subtitle); ?></p>
            </div>
        </div>
    </div>
</section>
<section class="w-full pb-8">
    <div class="container px-4 mx-auto sm:px-12 xl:px-48 fondo-fin">

        <?php echo $about->content; ?>



    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/u585332357/domains/krossov.tv/public_html/laravel/themes/krossov/views/about/index.blade.php ENDPATH**/ ?>