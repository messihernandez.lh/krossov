-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-12-2021 a las 14:54:02
-- Versión del servidor: 10.5.12-MariaDB-0+deb11u1
-- Versión de PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `krossov`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_rules`
--

CREATE TABLE `acl_rules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_06_174631_make_acl_rules_table', 2),
(4, '2019_08_19_000000_create_failed_jobs_table', 2),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 2),
(6, '2021_11_01_162800_add_timezone_column_to_users_table', 2),
(7, '2021_11_01_163616_project', 2),
(9, '2021_11_01_164945_project_text', 2),
(10, '2021_11_01_165049_project_video', 3),
(11, '2021_11_01_164914_project_gallery', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE `project` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isvideo` tinyint(1) NOT NULL DEFAULT 0,
  `urlvideo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isurl` tinyint(1) NOT NULL DEFAULT 0,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contentOrden` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credits` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isvisible` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `portada` int(4) DEFAULT NULL,
  `bgcolor` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`id`, `title`, `subtitle`, `content`, `images`, `isvideo`, `urlvideo`, `isurl`, `url`, `contentOrden`, `credits`, `color`, `isvisible`, `created_at`, `updated_at`, `portada`, `bgcolor`) VALUES
(1, 'Blend', NULL, 'Bienvenidos a la 500a Conferencia Blend bianual que se celebrará en el Anfiteatro Utopia Basin en Marte este año. Celebraremos los hitos que tanto la humanidad como MoGraphers como usted han logrado desde nuestra primera conferencia en 2015. Como\r\nun retroceso al pasado, queremos que animen algunos tableros creados por Lucas Brooking y Gareth O\'Brien. Estos tableros tienen una apariencia similar a la animación original que se creó para ese primer Blend.\r\nNota: No hay guión ni voz en off para esta pieza. Le hemos proporcionado algunas opciones de música con marca de agua, pero siéntase\r\nlibre de elegir la que más le convenga.', '{\"1\":\"Blend3015-Styleframe-01.jpg\",\"2\":\"Blend3015-Styleframe-05.jpg\",\"3\":\"Blend3015-Styleframe-06.jpg\"}', 0, NULL, 0, NULL, '[{\"orden\":1,\"type\":\"gallery\",\"id\":1},{\"orden\":2,\"type\":\"text\",\"id\":1}]', '---', NULL, 1, '2021-11-13 17:32:30', '2021-11-13 17:32:30', 2, NULL),
(2, 'Focus IO', NULL, 'Nuestra misión en Focus.io es ayudar a las personas a alejarse de las distracciones en sus dispositivos digitales y concentrarse en lo que es realmente importante. En este momento, estamos ejecutando una campaña en Instagram con algunos de nuestros consejos rápidos para que las personas se interesen en cómo podemos\r\nayudarlas a estar más concentradas y productivas a lo largo del día, y dejar de distraerse con su teléfono.\r\n', '{\"1\":\"Focus IO - 02.png\",\"2\":\"Focus IO - 05.png\",\"3\":\"Focus IO - 06.png\"}', 1, NULL, 0, NULL, '[{\"orden\":1, \"id\":2,\"type\":\"gallery\"},{\"orden\":2, \"id\":4,\"type\":\"text\"},{\"orden\":3, \"id\":1,\"type\":\"video\"},{\"orden\":4, \"id\":3,\"type\":\"gallery\"}]', '----', NULL, 1, '2021-11-13 17:32:30', '2021-11-13 17:32:30', 2, NULL),
(3, 'Semilla tallo', NULL, 'Las plantas de interior tienen muchos beneficios diferentes, pero muchas personas no tienen ni idea de cómo mantenerlas vivas. En Seed & Stem brindamos consejos de cuidado\r\ny educación para que las personas sepan cómo mantener vivas sus plantas.\r\n\r\nNecesitamos un anuncio de 15 a 20 segundos creado para su uso en varios medios de comunicación social para que la gente se interese en nuestra misión en Seed & Stem. Le proporcionamos un guión, una voz y algunos tableros que fueron diseñados por la talentosa Sarah Beth.', '{\"1\":\"Seed_Stem-Styleframe-01.png\",\"2\":\"Seed_Stem-Styleframe-03.png\",\"3\":\"Seed_Stem-Styleframe-05.png\"}', 0, NULL, 0, NULL, '[{\"orden\":1,\"id\":7,\"type\":\"gallery\"},{\"orden\":2,\"id\":10,\"type\":\"text\"},{\"orden\":3,\"id\":8,\"type\":\"gallery\"}]', '----', NULL, 1, '2021-11-13 17:34:50', '2021-11-13 17:34:50', 2, NULL),
(4, 'Museum Milano', NULL, NULL, '{\"1\":\"Armon\\u00eda-02.jpg\",\"2\":\"Divisi\\u00f3n-Asim\\u00e9trica-01.jpg\",\"3\":\"Simetr\\u00eda-02.jpg\"}', 1, NULL, 0, NULL, '[{\"orden\":1,\"id\":2,\"type\":\"video\"},{\"orden\":2,\"id\":7,\"type\":\"text\"},{\"orden\":3,\"id\":9,\"type\":\"gallery\"},{\"orden\":4,\"id\":8,\"type\":\"text\"},{\"orden\":5,\"id\":10,\"type\":\"gallery\"},{\"orden\":6,\"id\":9,\"type\":\"text\"},{\"orden\":7,\"id\":11,\"type\":\"gallery\"}]', NULL, NULL, 1, '2021-11-13 17:34:50', '2021-11-13 17:34:50', 2, NULL),
(5, 'Motion Design DNA', NULL, 'Science Addicts Unite es un sitio que se está lanzando para enseñar ciencia de formas más modernas y atractivas. Producirán un formato largo y un mini contenido diseñado para involucrar a los alumnos al mostrar lo genial que puede ser la ciencia. \r\nEste anuncio de aproximadamente 20 segundos se publicará en YouTube y las redes sociales, y realmente debería sorprender al espectador con el diseño, la animación y el mensaje. No hay un llamado a la acción claro que no sea mostrar la URL en la tarjeta final porque queremos atraer personas curiosas al sitio. ', '{\"1\":\"Motion-Design-DNA-Preview-01.jpg\",\"2\":\"Motion-Design-DNA-Preview-04.jpg\",\"3\":\"Motion-Design-DNA-Preview-05.jpg\"}', 0, NULL, 0, NULL, '[{\"orden\":1,\"id\":5,\"type\":\"text\"},{\"orden\":2,\"id\":6,\"type\":\"gallery\"},{\"orden\":3,\"id\":6,\"type\":\"text\"}]', '---', NULL, 1, '2021-11-13 17:36:28', '2021-11-13 17:36:28', 2, NULL),
(6, 'Busy Bee', 'Los polinizadores como las abejas polinizan un tercio de los cultivos que comemos.', 'Mucha gente no sabe qué impacto tienen las abejas en su vida diaria y en los alimentos que comen. En Buzzlove, queremos cambiar eso creando conciencia ...', '{\"1\":\"SoM_PL_002.png\",\"2\":\"SoM_PL_010.png\",\"3\":\"SoM_PL_003.png\"}', 0, NULL, 0, NULL, '[{\"orden\":1,\"id\":9,\"type\":\"text\"},{\"orden\":2,\"id\":4,\"type\":\"gallery\"},{\"orden\":3,\"id\":10,\"type\":\"text\"},{\"orden\":4,\"id\":5,\"type\":\"gallery\"}]', '-----', NULL, 1, '2021-11-13 17:36:28', '2021-11-13 17:36:28', 2, NULL),
(7, 'sadasd', '', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem earum officia sequi pariatur? Quaerat eum autem quisquam nostrum illo expedita.', '{\"1\":\"1638427060anime-anime-girls-illustration-dark-background-monochrome-night-1569187-wallhere.com.jpg\",\"2\":\"16384270601584524.jpg\",\"3\":\"16384270604232967.jpg\"}', 0, '', 0, '', '[{\"id\":11,\"orden\":1,\"type\":\"video\"},{\"id\":15,\"orden\":2,\"type\":\"gallery\"},{\"id\":12,\"orden\":3,\"type\":\"text\"}]', NULL, '#000000', 0, '2021-12-02 12:37:40', '2021-12-02 14:21:54', 3, '#ffffff');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projectgallery`
--

CREATE TABLE `projectgallery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projectid` bigint(20) UNSIGNED NOT NULL,
  `projectorden` int(11) NOT NULL,
  `imgs` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projectgallery`
--

INSERT INTO `projectgallery` (`id`, `projectid`, `projectorden`, `imgs`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '{\"class\":{\"gridCols\":3,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Blend3015-Styleframe-01.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Blend3015-Styleframe-02.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Blend3015-Styleframe-03.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Blend3015-Styleframe-04.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Blend3015-Styleframe-05.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Blend3015-Styleframe-06.jpg\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:14:52', '2021-12-01 10:28:32'),
(2, 2, 1, '{\"class\":{\"gridCols\":5,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 01.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 02.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 03.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 04.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 05.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 06.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 07.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 08.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 09.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 10.png\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:15:27', '2021-12-01 10:49:26'),
(3, 2, 4, '{\"class\":{\"gridCols\":4,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-1\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 11.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-1\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 12.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 13.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 14.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 15.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 16.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 17.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Focus IO - 18.png\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:15:27', '2021-12-01 11:37:40'),
(4, 6, 2, '{\"class\":{\"gridCols\":12,\"gridRows\":0},\"img\":[{\"colSpan\":4,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-4\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_001.png\",\"class\":\"col-span-1\"},{\"colSpan\":4,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-4\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_002.png\",\"class\":\"col-span-1\"},{\"colSpan\":4,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-4\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_003.png\",\"class\":\"col-span-1\"},{\"colSpan\":6,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-6\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_004.png\",\"class\":\"col-span-1\"},{\"colSpan\":6,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-6\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_005.png\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:20:18', '2021-12-01 11:23:03'),
(5, 6, 4, '{\"class\":{\"gridCols\":4,\"gridRows\":2},\"img\":[{\"colSpan\":2,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-2\",\"rows\":\"row-span-1\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":1,\"posicionYFin\":3,\"src\":\"SoM_PL_006.png\",\"class\":\"col-span-1\"},{\"colSpan\":2,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-2\",\"rows\":\"row-span-1\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_007.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-1\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_008.png\",\"class\":\"col-span-1\"},{\"colSpan\":2,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-2\",\"rows\":\"row-span-1\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"SoM_PL_008.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":1,\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-1\"},\"posicionXIni\":3,\"posicionXFin\":5,\"posicionYIni\":2,\"posicionYFin\":2,\"src\":\"SoM_PL_010.png\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:20:18', '2021-12-01 11:22:13'),
(6, 5, 2, '{\"class\":{\"gridCols\":4,\"gridRows\":2},\"img\":[{\"colSpan\":2,\"rowSpan\":2,\"classes\":{\"columns\":\"col-span-2\",\"rows\":\"row-span-2\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Motion-Design-DNA-Preview-01.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Motion-Design-DNA-Preview-02.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Motion-Design-DNA-Preview-03.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Motion-Design-DNA-Preview-04.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Motion-Design-DNA-Preview-05.jpg\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:24:01', '2021-12-01 11:32:19'),
(7, 3, 1, '{\"class\":{\"gridCols\":2,\"gridRows\":2},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-01.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-02.png\",\"class\":\"col-span-1\"},{\"colSpan\":2,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-2\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-03.png\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:24:01', '2021-12-01 11:35:41'),
(8, 3, 3, '{\"class\":{\"gridCols\":4,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-04.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-05.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-06.png\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Seed_Stem-Styleframe-07.png\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:29:28', '2021-12-01 11:11:51'),
(9, 4, 3, '{\"class\":{\"gridCols\":2,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Armonu00eda-01.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Armonu00eda-02.jpg\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:31:15', '2021-12-01 11:17:18'),
(10, 4, 5, '{\"class\":{\"gridCols\":2,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Divisiu00f3n-Asimu00e9trica-01.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Divisiu00f3n-Asimu00e9trica-02.jpg\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:31:15', '2021-12-01 11:17:27'),
(11, 4, 7, '{\"class\":{\"gridCols\":2,\"gridRows\":0},\"img\":[{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Simetru00eda-01.jpg\",\"class\":\"col-span-1\"},{\"colSpan\":1,\"rowSpan\":\"auto\",\"classes\":{\"columns\":\"col-span-1\",\"rows\":\"row-span-auto\"},\"posicionXIni\":\"auto\",\"posicionXFin\":\"auto\",\"posicionYIni\":\"auto\",\"posicionYFin\":\"auto\",\"src\":\"Simetru00eda-02.jpg\",\"class\":\"col-span-1\"}]}', '2021-11-15 22:31:15', '2021-12-01 11:17:42'),
(12, 7, 3, '', '2021-12-02 13:58:33', '2021-12-02 13:58:33'),
(13, 7, 4, '', '2021-12-02 13:58:49', '2021-12-02 13:58:49'),
(14, 7, 3, '', '2021-12-02 14:00:48', '2021-12-02 14:00:48'),
(15, 7, 2, '', '2021-12-02 14:21:43', '2021-12-02 14:21:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projecttext`
--

CREATE TABLE `projecttext` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projectid` bigint(20) UNSIGNED NOT NULL,
  `projectorden` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bgcolor` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projecttext`
--

INSERT INTO `projecttext` (`id`, `projectid`, `projectorden`, `content`, `created_at`, `updated_at`, `bgcolor`) VALUES
(1, 1, 2, '<p class=\"text-white text-justify\">\r\n            Bienvenidos a la 500a Conferencia Blend bianual que se celebrará en el Anfiteatro Utopia Basin en Marte este año. Celebraremos los hitos que tanto la humanidad como MoGraphers como usted han logrado desde nuestra primera conferencia en 2015. Como\r\nun retroceso al pasado, queremos que animen algunos tableros creados por Lucas Brooking y Gareth O\'Brien. Estos tableros tienen una apariencia similar a la animación original que se creó para ese primer Blend.\r\nNota: No hay guión ni voz en off para esta pieza. Le hemos proporcionado algunas opciones de música con marca de agua, pero siéntase\r\nlibre de elegir la que más le convenga.\r\n        </p>', '2021-11-15 20:21:16', '2021-11-15 20:21:16', '#000000'),
(2, 6, 3, '<p class=\"text-justify text-white\">\r\n            Mucha gente no sabe qué impacto tienen las abejas en su vida diaria y en los alimentos que comen. En Buzzlove, queremos cambiar eso creando\r\nconciencia de lo importantes que son las abejas al educar a los ciudadanos sobre cuán críticas son las abejas para nuestros sistemas alimentarios y el medio ambiente, por qué están siendo amenazadas y qué pueden hacer para ayudar.\r\nQueremos que cree un anuncio de 15 segundos para que se publique en varios medios de comunicación social para que la gente se interese en nuestra misión en Buzzlove.<br><br>\r\nLe proporcionamos un guión, VO y también algunos tableros que fueron diseñados.\r\n        </p>', '2021-11-15 20:25:24', '2021-11-15 20:25:24', NULL),
(3, 6, 1, '<p class=\"text-justify text-white\">\r\n            Los polinizadores como las abejas polinizan un tercio de los cultivos que comemos. Pero nuestros amiguitos están desapareciendo y sin ellos... No hay más manzanas.\r\nNo más almendras.\r\nNo más aguacates.\r\nAyude a nuestros amigos voladores plantando jardines. Y\r\ndirígete a buzzlove.co para descubrir cómo.\r\n        </p>', '2021-11-15 20:25:24', '2021-11-15 20:25:25', NULL),
(4, 2, 2, '<p class=\"text-justify text-white\">Nuestra misión en Focus.io es ayudar a las personas a alejarse de las distracciones en sus dispositivos digitales y concentrarse en lo que es realmente importante. En este momento, estamos ejecutando una campaña en Instagram con algunos de nuestros consejos rápidos para que las personas se interesen en cómo podemos\r\nayudarlas a estar más concentradas y productivas a lo largo del día, y dejar de distraerse con su teléfono.</p>', '2021-11-15 20:30:28', '2021-11-15 20:30:28', NULL),
(5, 5, 1, '<p class=\"text-center text-white\">\r\nMire dentro de una celda y descubrirá que tiene su propio idioma.<br>\r\nVerá, los genes son como letras en una máquina de escribir. <br>\r\nSe combinan para formar palabras y oraciones. <br>\r\nConecta suficientes de ellos y surge una historia.<br> \r\nLa historia de quien eres. Y si crees que es genial ...<br> \r\nBueno, entonces eres uno de nosotros.<br>\r\nLos adictos a la ciencia se unen \r\n</p>', '2021-11-15 20:30:28', '2021-11-15 20:30:28', NULL),
(6, 5, 3, '<p class=\"text-justify text-white\">\r\nScience Addicts Unite es un sitio que se está lanzando para enseñar ciencia de formas más modernas y atractivas. Producirán un formato largo y un mini contenido diseñado para involucrar a los alumnos al mostrar lo genial que puede ser la ciencia. \r\nEste anuncio de aproximadamente 20 segundos se publicará en YouTube y las redes sociales, y realmente debería sorprender al espectador con el diseño, la animación y el mensaje. No hay un llamado a la acción claro que no sea mostrar la URL en la tarjeta final porque queremos atraer personas curiosas al sitio. \r\n<p>', '2021-11-15 20:33:26', '2021-11-15 20:33:26', NULL),
(7, 4, 2, '<h3 class=\"text-center font-semibold text-2xl text-white\">\r\n            Armonia\r\n        </h3>\r\n        <br>\r\n<p class=\"text-justify text-white\">\r\nEl Museo de Milán está llevando a cabo una exposición del trabajo de la artista Isabella Conticello y le gustaría ver su arte cobrar vida y contar una historia que se relacione con una de sus citas famosas. Las palabras de la cita solo se escucharán a través de VO que se le proporciona y no se mostrarán en la pantalla. \r\n</p>\r\n<h3 class=\"text-center font-semibold text-xl text-white\">\r\n            Cita de Isabella \r\n        </h3>\r\n        <br>\r\n<p class=\"text-justify text-white\">\r\n\"La armonía se logra, no cuando no hay nada más que agregar, sino cuando no queda nada para quitar\". \r\n</p>', '2021-11-15 20:33:26', '2021-11-15 20:33:26', NULL),
(8, 4, 4, '<h3 class=\"text-center font-semibold text-2xl text-white\">\r\n            División Asimétrica\r\n        </h3>\r\n        <br>\r\n<p class=\"text-justify text-white\">\r\nEl Museo de Milán está llevando a cabo una exposición del trabajo de la artista Isabella Conticello y le gustaría ver su arte cobrar vida y contar una historia que se relacione con una de sus citas famosas. Las palabras de la cita solo se escucharán a través de VO que se le proporciona y no se mostrarán en la pantalla.  \r\n</p>\r\n<h3 class=\"text-center font-semibold text-xl text-white\">\r\n            Cita de Isabella \r\n        </h3>\r\n        <br>\r\n<p class=\"text-justify text-white\">\r\n“Se necesita una división asimétrica para crear la dinámica necesaria para la progresión y extensión desde la unidad”. </p>', '2021-11-15 20:38:42', '2021-11-15 20:38:42', NULL),
(9, 4, 6, '<h3 class=\"text-center font-semibold text-2xl text-white\">\r\n            Simetría\r\n        </h3>\r\n        <br>\r\n<p class=\"text-justify text-white\">\r\nEl Museo de Milán está llevando a cabo una exposición del trabajo de la artista Isabella Conticello y le gustaría ver su arte cobrar vida y contar una historia que se relacione con una de sus citas famosas. Las palabras de la cita solo se escucharán a través de VO que se le proporciona y no se mostrarán en la pantalla.  \r\n</p>\r\n<h3 class=\"text-center font-semibold text-xl text-white\">\r\n            Cita de Isabella \r\n        </h3>\r\n        <br>\r\n<p class=\"text-justify text-white\">\r\n\"La simetría, es una idea mediante la cual la humanidad ha tratado de comprender y crear orden, belleza y perfección\". </p>', '2021-11-15 20:38:42', '2021-11-15 20:38:42', NULL),
(10, 3, 2, '<p class=\"text-justify text-white\">\r\n            Las plantas de interior tienen muchos beneficios diferentes, pero muchas personas no tienen ni idea de cómo mantenerlas vivas. En Seed & Stem brindamos consejos de cuidado\r\ny educación para que las personas sepan cómo mantener vivas sus plantas.\r\n<br><br>\r\nNecesitamos un anuncio de 15 a 20 segundos creado para su uso en varios medios de comunicación social para que la gente se interese en nuestra misión en Seed & Stem. Le proporcionamos un guión, una voz y algunos tableros que fueron diseñados por la talentosa Sarah Beth.\r\n        </p>', '2021-11-15 20:43:50', '2021-11-15 20:43:50', NULL),
(11, 7, 2, '', '2021-12-02 13:58:19', '2021-12-02 13:58:19', '#000000'),
(12, 7, 3, '', '2021-12-02 14:21:54', '2021-12-02 14:21:54', '#903131');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projectvideo`
--

CREATE TABLE `projectvideo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projectid` bigint(20) UNSIGNED NOT NULL,
  `projectorden` int(11) NOT NULL,
  `urlvideo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projectvideo`
--

INSERT INTO `projectvideo` (`id`, `projectid`, `projectorden`, `urlvideo`, `class`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'https://vimeo.com/349333172', '349333172', '2021-11-15 20:49:59', '2021-12-02 10:27:48'),
(2, 4, 1, 'https://vimeo.com/349333172', '349333172', '2021-11-15 20:49:59', '2021-12-02 10:27:04'),
(3, 7, 4, '', '', '2021-12-02 13:25:42', '2021-12-02 13:25:42'),
(4, 7, 1, '', '', '2021-12-02 13:34:40', '2021-12-02 13:34:40'),
(5, 7, 1, '', '', '2021-12-02 13:35:48', '2021-12-02 13:35:48'),
(6, 7, 1, '', '', '2021-12-02 13:37:45', '2021-12-02 13:37:45'),
(7, 7, 1, '', '', '2021-12-02 13:40:31', '2021-12-02 13:40:31'),
(8, 7, 1, '', '', '2021-12-02 13:41:08', '2021-12-02 13:41:08'),
(9, 7, 1, '', '', '2021-12-02 14:13:16', '2021-12-02 14:13:16'),
(10, 7, 2, '', '', '2021-12-02 14:14:17', '2021-12-02 14:14:17'),
(11, 7, 1, '', '', '2021-12-02 14:21:38', '2021-12-02 14:21:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acl_rules_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `projectgallery`
--
ALTER TABLE `projectgallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projectgallery_projectid_foreign` (`projectid`);

--
-- Indices de la tabla `projecttext`
--
ALTER TABLE `projecttext`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projecttext_projectid_foreign` (`projectid`);

--
-- Indices de la tabla `projectvideo`
--
ALTER TABLE `projectvideo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projectvideo_projectid_foreign` (`projectid`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acl_rules`
--
ALTER TABLE `acl_rules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `projectgallery`
--
ALTER TABLE `projectgallery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `projecttext`
--
ALTER TABLE `projecttext`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `projectvideo`
--
ALTER TABLE `projectvideo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD CONSTRAINT `acl_rules_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `projectgallery`
--
ALTER TABLE `projectgallery`
  ADD CONSTRAINT `projectgallery_projectid_foreign` FOREIGN KEY (`projectid`) REFERENCES `project` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `projecttext`
--
ALTER TABLE `projecttext`
  ADD CONSTRAINT `projecttext_projectid_foreign` FOREIGN KEY (`projectid`) REFERENCES `project` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `projectvideo`
--
ALTER TABLE `projectvideo`
  ADD CONSTRAINT `projectvideo_projectid_foreign` FOREIGN KEY (`projectid`) REFERENCES `project` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
