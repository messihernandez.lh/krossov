<?php

use App\Http\Controllers\FrontendController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\AboutPageController;
use App\Http\Controllers\ContactPageController;
use App\Http\Controllers\ProjectGalleryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/home', function () {
//     return view('home');
// })->middleware(['auth'])->name('home');

Route::group(['middleware' => 'theme:krossov'], function () {
    Auth::routes(['register' => false]);
    // Auth::routes();
    Route::get('/', [FrontendController::class, 'index'])->name('index');
    Route::get('/project', [FrontendController::class, 'projects'])->name('project.home');
    Route::get('/jobs', [FrontendController::class, 'jobs'])->name('project.jobs');
    Route::get('/about', [FrontendController::class, 'about'])->name('project.about');
    Route::get('/contact', [FrontendController::class, 'contact'])->name('project.contact');
    Route::get('/project/{id}', [FrontendController::class, 'project'])->name('project');

    // require __DIR__ . '/auth.php';
});

Route::prefix("admin")->group(function () {

    Route::group(['middleware' => ['theme:admin']], function () {
    // Route::group(['middleware' => ['theme:admin','auth']], function () {

        Route::get("/",function(){
            return redirect(route('home.index'));
        });
        Route::resource('project', ProjectController::class);
        Route::post('projectgallery/add',[ProjectGalleryController::class,'add']);
        Route::resource('home', HomePageController::class);
        Route::resource('about', AboutPageController::class);
        Route::resource('contact', ContactPageController::class);
        Route::get('dashboard', function () {
            return redirect(route('project.index'));
        })->name('admin.dashboard');

        // Route::get('project/create', function () {
        //     return view('projects.create');
        // })->name('admin.project.create');

        // Route::post('project/create', function (Request $request) {
        //     dd($request);
        // })->name('admin.project.store');
    });
});



Route::get('/home', function(){
    return redirect(route('admin.dashboard'));
})->name('home');
