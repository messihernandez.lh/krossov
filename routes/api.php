<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Tightenco\Ziggy\Ziggy;
use App\Http\Controllers\ProjectGalleryController;
use App\Http\Controllers\ProjectVideoController;
use App\Http\Controllers\ProjectTextController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\FrontendController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('ziggy', function (Request $request) {
    return response()->json(new Ziggy);
});



// Route::apiResource('project', ProjectController::class);
Route::apiResource('project.projectgallery', ProjectGalleryController::class);
Route::apiResource('project.projecttext', ProjectTextController::class);
Route::apiResource('project.projectvideo', ProjectVideoController::class);
Route::put('project/{id}', [ProjectController::class, 'upgrade'])->name('project.upgrade');
/*
Route::post('project/{project}/projectgallery/upgrade/', [ProjectGalleryController::class, 'upgrade'])->name('projectgallery.upgrade');*/

Route::post("mail/send", [FrontendController::class, 'sendMails'])->name("sendmail");
