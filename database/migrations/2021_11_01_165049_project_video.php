<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProjectVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projectvideo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('projectid');
            $table->integer('projectorden');
            $table->text('urlvideo');
            $table->text('class');
            $table->timestamps();
            $table->foreign('projectid')->references('id')->on('project')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectvideo');
    }
}
