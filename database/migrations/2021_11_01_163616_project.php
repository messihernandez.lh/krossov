<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Project extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->id();
            $table->string('title', 200);
            $table->string('subtitle', 200);
            $table->mediumText('content');
            $table->boolean('isvideo')->default(false);
            $table->text('urlvideo');
            $table->boolean('isurl')->default(false);
            $table->text('url');
            $table->text('contentOrden');
            $table->longText('credits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}


// Client: Google
// Directed by: Oddfellows
// Executive Creative Direction: Colin Trenter
// Executive Producer: Erica Kelly
// Creative Direction: Jarratt Moody
// Producer: Dennis Samatulski
// Design: Yuki Yamada, Oscar Pettersson, Alisha Liu, Caroline Choi
// Animation: Tyler Morgan, Mark Lundgren, Jon Riedell, Jakob Scott
// Writing : Inside Company
// Music : Sounds Delicious
// SFX: Ambrose Yu