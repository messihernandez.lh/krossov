<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-theme="krossov">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />

    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <link rel="stylesheet" href="{{ asset('themes/krossov/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/krossov/css/zoom.css') }}">

    <title>krossov</title>

    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@cycjimmy/swiper-animation@4/dist/swiper-animation.umd.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/plugins/animation.gsap.js" integrity="sha512-judXDFLnOTJsUwd55lhbrX3uSoSQSOZR6vNrsll+4ViUFv+XOIr/xaIK96soMj6s5jVszd7I97a0H+WhgFwTEg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @stack('css')
    @isset($fondo)
    <style>
        body{
            background-color:{{ $fondo }} !important;
            color:{{ $textcolor }} !important;
        }
        footer{
            background-color:{{ $fondo }} !important;
            color:{{ $textcolor }} !important;
        }
</style>
@endisset
@isset($home)
<style>
        .sidebar{
            background-color:{{ $home->fondomenu }} !important;
            color:{{ $home->textomenu }} !important;
        }
    </style>
    @endisset
</head>

@if(!isset($fondo) && !isset($textcolor))

<body class="bg-gray-100 text-black" >
@else

<body class="{{ $fondo!==''?'':'bg-gray-100' }} {{ $textcolor!==''?'':'text-black' }}" >
@endif
    <div id="barra-menu" class="fixed right-0 w-4 h-screen bg-transparent z-100"></div>
    <div>
        <header>
            <button id="menuburger" class="fixed z-50 bg-base-content menu-burger right-4 top-4">
                <i class="bg-gray-100"></i>
                <i class="bg-gray-100"></i>
                <i class="bg-gray-100"></i>
            </button>
            <div class="gap-4 sidebar z-70">
                <div class="flex flex-row-reverse">
                    <button class="hidden w-12 h-12 transition-all delay-300 bg-base-content close-menu">
                        <i class='text-xl text-gray-100 bx bx-x'></i>
                    </button>

                </div>
                <div class="flex flex-wrap items-center justify-center p-4">
                    <img src="{{ asset('favicon.png') }}" class="w-20" alt="logo">
                </div>
                <ul class="flex flex-col w-full gap-5">
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="{{route('index')}}">Home</a></li>
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="{{ route('project.home') }}">Work</a></li>
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="{{ route('project.about') }}">About</a></li>
                    <li class="w-full font-sans text-xl  text-center cursor-pointer lg:text-3xl hover:scale-110 ">
                        <a href="{{ route('project.contact') }}">Contact</a></li>
                    {{--  <li class="w-full font-sans text-xl font-semibold text-center text-gray-500 cursor-pointer lg:text-3xl hover:scale-110 hover:text-gray-800">
                        <a href="{{ route('project.jobs') }}">Jobs</a></li>  --}}
                </ul>
                <ul class="absolute overflow-hidden flex justify-center w-full gap-3 py-4 bottom-2">
                    @isset($socialNetwork)
                    @foreach($socialNetwork as $network)

                    <li>
                        <a href="{{$network->url  }}" target="_blank" class="text-xl" rel="nofollow">
                            {!! $network->icon !!}
                        </a>
                    </li>
                    @endforeach
                    @endisset
                </ul>
            </div>
        </header>
        <main class="min-h-screen overflow-hidden w-full">
            <div class="" id="app">
                @yield('content')

            </div>
        </main>
        <footer class="relative z-20 w-full max-h-screen py-8">
            <div class="container flex flex-wrap justify-center sm:px-12">
                <div class="flex flex-col lg:flex-row items-center justify-between w-full">
                    <p class="text-lg"><i class='bx bx-copyright'></i> Derechos Reservados {{now()->year}}</p>
                    <div class="flex-grow"></div>
                    <ul class="flex justify-center gap-3 py-4">
                        @isset($socialNetwork)
                        @foreach($socialNetwork as $network)

                    <li>
                        <a href="{{$network->url  }}" target="_blank" class="text-xl" rel="nofollow">
                            {!! $network->icon !!}
                        </a>
                    </li>
                    @endforeach
                    @endisset
                        {{--  <li>
                            <a href="https://www.behance.net/krossovtv" target="_blank" class="text-xl" rel="nofollow">
                                <i class='text-gray-400 text-xl bx hover:text-gray-800 bxl-behance'></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/krossovtv/" target="_blank" class="text-xl" rel="nofollow">
                                <i class='text-gray-400 text-xl bx hover:text-gray-800 bxl-instagram'></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://dribbble.com/krossovtv" target="_blank" class="text-xl" rel="nofollow">
                                <i class='text-gray-400 text-xl bx hover:text-gray-800 bxl-dribbble'></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://vimeo.com/user94847387" target="_blank" class="text-xl" rel="nofollow">
                                <i class='text-gray-400 text-xl bx hover:text-gray-800 bxl-vimeo'></i>
                            </a>
                        </li>  --}}
                    </ul>
                    <div class="flex-grow"></div>
                    <div class="flex items-center gap-3">
                        <p class="text-sm font-bold uppercase">Power by:</p>
	<a href="https://redirect.monol4b.com/" target="_blank">
                            <img src="{{ asset('logos/gris.png') }}" class="w-36" alt="krossov" />
</a>
                    </div>
                </div>
                <div class="flex justify-end w-full">
                </div>
            </div>
        </footer>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('themes/krossov/js/zoom.min.js') }}"></script>
    @stack('script')
    <script>
        window.addEventListener('load', () => {

            document.querySelector("#menuburger").addEventListener('click', () => {
                document.querySelector('.sidebar').classList.toggle('active');
                window.isEnableMenu = true;
                document.querySelector('.close-menu').classList.remove('hidden');
                document.body.classList.add("overflow-hidden");
            });
            document.querySelector(".close-menu").addEventListener('click', () => {
                document.querySelector('.sidebar').classList.toggle('active');
                document.querySelector('.close-menu').classList.add('hidden');
                window.isEnableMenu = false;
                document.body.classList.remove("overflow-hidden");
            });
        });

    </script>

    <script async src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.4.0/dist/lazyload.min.js"></script>
    <script>
        function logElementEvent(eventName, element) {
            console.log(Date.now(), eventName, element.getAttribute("data-src"));
        }

        var callback_enter = function(element) {
            logElementEvent("🔑 ENTERED", element);
        };
        var callback_exit = function(element) {
            logElementEvent("🚪 EXITED", element);
        };
        var callback_loading = function(element) {
            logElementEvent("⌚ LOADING", element);
        };
        var callback_loaded = function(element) {
            logElementEvent("👍 LOADED", element);
        };
        var callback_error = function(element) {
            logElementEvent("💀 ERROR", element);
            element.src =
                "https://via.placeholder.com/440x560/?text=Error+Placeholder";
        };
        var callback_finish = function() {
            logElementEvent("✔️ FINISHED", document.documentElement);
        };
        var callback_cancel = function(element) {
            logElementEvent("🔥 CANCEL", element);
        };

        window.lazyLoadOptions = {
            threshold: 0,

            callback_enter: callback_enter
            , callback_exit: callback_exit
            , callback_cancel: callback_cancel
            , callback_loading: callback_loading
            , callback_loaded: callback_loaded
            , callback_error: callback_error
            , callback_finish: callback_finish
        };
        window.addEventListener('load', () => {
            let lazyLoadInstance = new LazyLoad(window.lazyLoadOptions);
        });
        window.addEventListener(
            "LazyLoad::Initialized"
            , function(e) {
                console.log(e.detail.instance);
            }
            , false
        );

    </script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('themes/krossov/js/app.js') }}"></script>


</body>

</html>
