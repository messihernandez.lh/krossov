<section class="hidden md:grid  w-full @if($gallery->class->gridRows>0) {{ 'grid-rows-'.$gallery->class->gridRows }} @endif {{ 'md:grid-cols-'.$gallery->class->gridCols }} mb-20 section mx-auto min-h-screen">
    @foreach ($gallery->img as $img)
    <div class="{{ $img->classes->columns }} @if($gallery->class->gridRows>0) {{ $img->classes->rows }} @endif">
        <img class="object-cover object-center w-full h-full lazy" data-src="{!! asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . html_entity_decode($img->src)) !!}" data-action="zoom" />
    </div>
    @endforeach
</section>

{{-- {{ $gallery->class }} --}}
