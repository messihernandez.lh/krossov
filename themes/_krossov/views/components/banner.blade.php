<section class="relative z-20 flex justify-center w-full overflow-hidden md:h-2/3vh lg:block" id="div-videos">

    <video class="object-cover object-center w-full h-full" preload="true" autoplay="true"
        loop="loop" muted>
        <source src="{{ asset('assets/about/'.$about->video) }}" type="video/mp4" />
    </video>

</section>
