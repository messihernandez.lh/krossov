@php
$images = json_decode($project->images, true);
$image=asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' .
$images[$project->portada]);

@endphp

<section class="relative w-full h-50">
    <img data-src="{!! asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . $images[$project->portada]) !!}"
        alt="" class="object-cover object-center w-full h-full lazy">
    
    <div class="absolute z-20 flex flex-col w-auto gap-4 bottom-4 left-4 flex-nowrap">
        <div class="w-full px-4 py-2 bg-gradient-to-t from-transparent to-gray-900">
            <h2 class="text-xl text-gray-100 uppercase text-shadow-sm">
                {{ $project->title }}
            </h2>

        </div>
        @if (intval($project->isvideo)===1)
        <button data-bs-toggle="modal" data-bs-target="#modalvideo"
            class="self-start mb-2 rounded-none btn btn-outline btn-secondary w-max">ver video</button>

        @endif
    </div>
</section>
