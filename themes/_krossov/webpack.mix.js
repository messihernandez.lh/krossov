const mix = require("laravel-mix");

require('laravel-mix-blade-reload');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.setPublicPath("public/themes/krossov")
    .js(`${__dirname}/js/app.js`, "js/app.js")
    .vue()
    .postCss(`${__dirname}/css/app.css`, "css/app.css", [
        require("postcss-import"),
        require("tailwindcss")({
            config: `${__dirname}/tailwind.config.js`,
        }),
        require("autoprefixer"),
    ])
    .version();
