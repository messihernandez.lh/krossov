const sideBar = {
    '.sidebar': {
        '@apply fixed flex w-0 flex-col h-screen top-0 left-0 z-50 bg-white dark:bg-gray-800 dark:text-white justify-between': {},
        transition: 'all 0.5s ease',
        '&>ul': {
            '&>li': {
                '@apply transition-all duration-300 delay-100 transform translate-x-10 ease-in-out': {}
            },
        },
        '&.active': {
            width: '18.5rem',
            '@apply p-4': {},
            '&>ul': {
                '&>li': {
                    '@apply translate-x-0 delay-0': {},
                },
            }
        },

    },
};

module.exports = sideBar;