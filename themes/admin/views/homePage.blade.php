@extends('layouts.app')
@section('content')
<form class="w-full" action="{{ route('home.update',['home' => $home->id]) }}" enctype="multipart/form-data" method="post">
    @csrf
    @method('PUT')
    <div class="container flex flex-col gap-4 px-4 mx-auto my-8 sm:px-12">
        <div class="flex items-center gap-2 md:w-96">
            <label for="color"  class="text-sm font-semibold ">Color de fondo</label>

            <input type="color" value="{{ $home->fondo }}" name="fondo" id="color" class="border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" value="#ffffff">
        </div>
        <div class="flex items-center gap-2 md:w-96">
            <label for="colortext" class="text-sm font-semibold ">Color del texto</label>

            <input type="color" value="{{ $home->texto }}" name="texto" id="colortext" class="border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" value="#000000">
        </div>
        <div class="flex items-center gap-2 md:w-96">
            <label for="color"  class="text-sm font-semibold ">Color de fondo del menu</label>

            <input type="color" value="{{ $home->fondomenu }}" name="fondomenu" id="color" class="border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" value="#ffffff">
        </div>
        <div class="flex items-center gap-2 md:w-96">
            <label for="colortext" class="text-sm font-semibold ">Color del texto del menu</label>

            <input type="color" value="{{ $home->textomenu }}" name="textomenu" id="colortext" class="border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" value="#000000">
        </div>
        <div class="flex items-center gap-3 md:w-96">
            <label for="video" class="text-sm font-semibold ">Video</label>
            <input type="file"  name="video" id="video" class="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Video de portada">
        </div>
        <div class="flex items-center gap-3 md:w-96">
            <label for="imagen" class="text-sm font-semibold ">Imagen</label>
            <input type="file"  name="imagen" id="imagen" class="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300" placeholder="Imagen">
        </div>
        <div class="flex flex-col w-full gap-2 my-4">
            <button class="px-4 py-2 text-sm font-medium text-white transition bg-blue-600 w-max hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"type="submit">Actualizar</button>
        </div>
    </div>
</form>
@endsection
