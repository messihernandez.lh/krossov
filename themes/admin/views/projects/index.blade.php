@extends('layouts.app')
@section('content')
<section class="w-full grid grid-cols-1 gap-3 py-8">
    <div class="container px-4 mx-auto">
        <div class="flex justify-end">
            <a class="px-4 py-2 text-sm font-medium text-white transition bg-blue-600 w-max hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                href="{{ route('project.create') }}">Nuevo Proyecto</a>
        </div>
        <div class="flex flex-col">
            <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="overflow-x-auto">
                        <table class="min-w-full">
                            <thead class="border-b">
                                <tr>
                                    <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                                        #
                                    </th>
                                    <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                                        Nombre
                                    </th>
                                    <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($projects as $key=>$project)
                                <tr class="border-b">
                                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">{{ $key+1 }}</td>
                                    <td class="text-sm font-light px-6 py-4 whitespace-nowrap">
                                        {{ $project->title }}
                                    </td>
                                    <td class="text-sm font-light px-6 py-4 whitespace-nowrap flex gap-3">
                                        <a class="px-4 py-2 text-sm font-medium text-white transition bg-blue-600 w-max hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                            href="{{ route('project.edit', ['project'=>$project->id]) }}">Editar</a>
                                        <button
                                            class="px-4 py-2 text-sm font-medium text-white transition bg-red-600 w-max hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
                                            data-bs-toggle="modal"
                                            data-bs-target="#modal-{{ $project->id }}">Eliminar</button>
                                        @include('projects.modal')
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-full">
            {{ $projects->links() }}
        </div>
    </div>
</section>
@endsection