@extends('layouts.app')
@section('content')
<section class="relative flex flex-col w-full min-w-full flex-nowrap">
    <div class="relative z-20 flex justify-center w-full overflow-hidden h-1/3vh md:h-2/3vh lg:block" id="div-videos">

        <video preload="true" autoplay="true" loop="loop" muted class="object-cover object-center w-full fixed h-1/3vh md:h-full" style="z-index:-999;">
            <source src="{{ asset('assets/video/teaser.mp4') }}" type="video/mp4" />
        </video>

    </div>
    <div id="div-logo" class="relative overflow-hidden z-20 flex items-center bg-gray-100 fondo-fin justify-center w-full h-50">
        <div class="container h-1/3 flex justify-center gap-4 mx-auto">

            <div>
                <img class="w-full max-w-lg relative z-100" src="{{ asset('logos/original.png') }}" alt="">
            </div>

        </div>
    </div>
</section>
@php
$count=App\Models\ProjectModel::where('isvisible',true)->where('isedit',0)->count();
@endphp

<div class="hidden bg-gray-100 fondo-fin z-20 relative w-full contenedor lg:block">
    @foreach (App\Models\ProjectModel::where('isvisible',true)->where('isedit',0)->get() as $item)
        @include('components.project')
    @endforeach
</div>
<div class="contenedor gap-3 bg-gray-100 fondo-fin relative z-20 flex flex-wrap justify-center lg:hidden w-full">
    @foreach (App\Models\ProjectModel::where('isvisible',true)->where('isedit',0)->get() as $item)
        @include('components.project-movil')
    @endforeach
</div>

@endsection
@push('script')
<script>
    window.addEventListener('load',()=>{


        let wh = window.innerHeight;
        let ctrl = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook: 'onLeave',
            }
        });
        document.querySelectorAll('.poyectos').forEach(el=>{
            let scene=new ScrollMagic.Scene({
                triggerElement: el,
                duration: 1000,
                trigerHook: 0.5,
                tweenChanges: true
            })
            .setPin(el)
            .addTo(ctrl);
            /*.addIndicators();*/
            scene.on("progress", function (event) {

                el.querySelector('.progressbar').style.width = event.progress.toFixed(2) * 100 + '%';
            });


            scene.on("start", function (event) {
                console.log("Hit start point of scene.");
            });
        });
    });

</script>
@endpush
