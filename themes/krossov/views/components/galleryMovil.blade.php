
<section class="relative w-full mb-20 overflow-hidden md:hidden">
    <div class="w-full" id="slider-{{ $orden->orden }}">

        <div class="swiper-wrapper">
            @foreach ($gallery->img as $img)
            @if (property_exists($img,'src'))
                <div class="swiper-slide">
                    <img class="object-cover object-center w-full h-full lazy"
                        src="{!! asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . $img->src) !!}" />
                </div>
            @endif
            @endforeach
        </div>
        <!-- Add Pagination -->
        <div
            class="absolute z-30 flex justify-center w-full gap-3 mt-8 swiper-pagination-banner-{{ $orden->orden }} bottom-4">
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next swiper-button-white text-primary btn-next-{{ $orden->orden }}"></div>
        <div class="swiper-button-prev swiper-button-white text-primary btn-prev-{{ $orden->orden }}"></div>
    </div>
</section>
@switch($typeSlide)
    @case(1)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: 'fade',
                            fadeEffect: {
                                crossFade: true,
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @case(2)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "flip",
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @case(3)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: ["-120%", 0, -500],
                                },
                                next: {
                                    shadow: true,
                                    translate: ["120%", 0, -500],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @case(4)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: ["-20%", 0, -1],
                                },
                                next: {
                                    translate: ["100%", 0, 0],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @case(5)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: [0, 0, -800],
                                    rotate: [180, 0, 0],
                                },
                                next: {
                                    shadow: true,
                                    translate: [0, 0, -800],
                                    rotate: [-180, 0, 0],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @case(6)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    translate: ["-125%", 0, -800],
                                    rotate: [0, 0, -90],
                                },
                                next: {
                                    shadow: true,
                                    translate: ["125%", 0, -800],
                                    rotate: [0, 0, 90],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @case(7)
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,
                            effect: "creative",
                            creativeEffect: {
                                prev: {
                                    shadow: true,
                                    origin: "left center",
                                    translate: ["-5%", 0, -200],
                                    rotate: [0, 100, 0],
                                },
                                next: {
                                    origin: "right center",
                                    translate: ["5%", 0, -200],
                                    rotate: [0, -100, 0],
                                },
                            },
                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
    @break
    @default
        @push('script')
            <script>
                window.addEventListener('load', () => {
                    try {
                        var swiperAnimation = new SwiperAnimation();
                        var swiper = new Swiper('#slider-{{ $orden->orden }}', {
                            spaceBetween: 0,
                            grabCursor: true,

                            autoplay: {
                                delay: 7600,
                                disableOnInteraction: false,
                                waitForTransition: true,
                            },
                            loop: true,
                            pagination: {
                                el: '.swiper-pagination-banner-{{ $orden->orden }}',
                                clickable: true,
                                type: 'custom',
                            },
                            navigation: {
                                nextEl: '.btn-next-{{ $orden->orden }}',
                                prevEl: '.btn-prev-{{ $orden->orden }}',
                            },
                            on: {
                                init: function() {
                                    swiperAnimation.init(this).animate();
                                },
                                slideChange: function() {
                                    swiperAnimation.init(this).animate();
                                }
                            }
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseenter', () => {
                            swiper.autoplay.stop();
                        });

                        document.querySelector('#slider-{{ $orden->orden }}').addEventListener('mouseleave', () => {
                            swiper.autoplay.start();
                        });
                    } catch (e) {}
                });
            </script>
        @endpush
@endswitch
