@php
$images = json_decode($item->images, true);
$portadax= $item->portada;
if($item->portada===0){
	$portadax= 1;
	}
@endphp
<div class="relative w-72 h-72 overflow-hidden">
    <a href="{{ route('project', ['id' => Str::replace(' ', '_', Str::lower($item->title))]) }}">
        <div class="relative w-full h-full">
            <img src="{!! asset('assets/projects/' . Str::replace(' ', '_', Str::lower($item->title)) . '/' . $images[$portadax]) !!}"
                alt="" class="z-20 object-cover object-center w-full h-full max-w-none">
                <div class="absolute bottom-0 left-0 block w-full h-full transition-all duration-300 bg-black bg-opacity-50 lg:hidden">
                    <h2 class="absolute text-gray-100 transition-all duration-150 delay-75 transform -translate-x-1/2 translate-y-1/2 bottom-1/2 left-1/2">{{ $item->title }}</h2>
                </div>
            
        </div>
    </a>
</div>

