
@if(array_key_exists('gridRows',$gallery->class) && array_key_exists('gridCols',$gallery->class))
<section class="hidden md:grid  w-full {{ 'grid-rows-'.$gallery->class->gridRows ?? '1' }}  {{ 'md:grid-cols-'.$gallery->class->gridCols ?? '1' }} mb-20 section mx-auto min-h-screen">
    @foreach ($gallery->img as $img)
    
    @if (property_exists($img,'src'))

    <div class="{{ $img->classes->columns ?? '' }} {{ $img->classes->rows??'' }}">
        <img class="object-cover object-center w-full h-full lazy" data-src="{!! asset('assets/projects/' . Str::replace(' ', '_', Str::lower($project->title)) . '/' . html_entity_decode($img->src)) !!}" data-action="zoom" />
    </div>
    @endif
    @endforeach
</section>
@endif
{{-- {{ $gallery->class }} --}}
