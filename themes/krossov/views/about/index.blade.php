@extends('layouts.app')
@section('content')
@include('components.banner')

<section>
    <div class="container flex flex-col items-center justify-center py-8 mx-auto">
        <img src="{{ asset('assets/about/'.$about->imagen) }}" class="w-44" alt="logo">
        <div class="flex justify-center w-full gap-5">
            <div class="w-px bg-gray-100 fondo-fin border-white"></div>
            <div class="py-2">
                <h2 class="text-xl font-semibold md:text-3xl">{{ $about->title }}</h2>
                <p class="text-sm italic md:text-xl">{{ $about->subtitle }}</p>
            </div>
        </div>
    </div>
</section>
<section class="w-full pb-8">
    <div class="container px-4 mx-auto sm:px-12 xl:px-48 fondo-fin">

        {!! $about->content !!}


    </div>
</section>
@endsection
