@extends('layouts.app')
@section('content')
    @include('components.banner')
    <section>
        <div class="container flex flex-col items-center justify-center py-8 mx-auto">
            <img src="{{ asset('favicon.png') }}" class="w-44" alt="logo">
            <div class="flex justify-center w-full gap-5">
                {{-- <div class="w-px bg-gray-100 border-white"></div>  --}}
                <div class="py-2">
                    <h2 class="text-xl font-semibold text-gray-800 md:text-4xl">Jobs</h2>

                </div>
            </div>
        </div>
    </section>
    <section class="w-full pb-8">
        <div class="container px-4 mx-auto sm:px-12 xl:px-48">
            <div class="pb-4">
                <p class="text-2xl text-gray-800">Looking for work?</p>
            </div>
            <p class="text-gray-800">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ea natus illum modi aspernatur repellendus aperiam velit mollitia amet accusamus voluptates.</p>
            <br>
            <p class="text-gray-800">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias rerum quae velit, veniam natus aliquid placeat debitis assumenda voluptas maiores, totam amet hic? Est, alias facere? Illo est ipsum soluta.</p>
            <br>
            <p class="text-gray-800">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium dicta autem deleniti doloremque impedit optio architecto aut eos pariatur. Id!</p>
        </div>
    </section>
@endsection
