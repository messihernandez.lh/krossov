@extends('layouts.app')
@section('content')
@if (intval($project->isvideo)===1)
        
        @include('components.modal')
        @endif
        
@include('components.bannerProject.bannerOne')
@if($project->contentOrden!=='')

    @foreach (json_decode($project->contentOrden) as $orden)

        @if ($orden->type === 'text')
            <section class="w-full max-w-5xl px-4 container relative z-30 mx-auto mb-20 sm:px-12 @if ($orden->orden === 1) pt-20 @endif">
                @php
                    $texto = App\Models\ProjectTextModel::where('projectId', $project->id)
                        ->where('projectOrden', $orden->orden)
                        ->first();
                @endphp
                {!! $texto->content??'' !!}
            </section>
        @endif
        @if ($orden->type === 'gallery')
            @php
                $gallery = App\Models\ProjectGalleryModel::where('projectId', $project->id)
                    ->where('projectOrden', $orden->orden)
                    ->first();
                $gallery = json_decode($gallery->imgs);

                $typeSlide = $orden->orden+1;
            @endphp
            @if ($gallery!=null)
            @include('components.gallery')
            @include('components.galleryMovil')
            @endif

        @endif
        @if ($orden->type === 'video')
            <section class="flex items-center justify-center w-full mb-20 md:mx-auto h-screen 2xl:container">
                @php
                    $video = App\Models\ProjectVideoModel::where('id', $orden->id)
                        ->first();

                @endphp
                <v-vimeo :autoplay="{{ json_encode(false) }}" :controls="{{ json_encode(false) }}"
                :loop="{{ json_encode(true) }}"
                :muted="{{ json_encode(true) }}"
                url="{{ $video->urlvideo }}"
                videoid="{{ $video->class }}"></v-vimeo>


            </section>
        @endif
    @endforeach
    @endif
    @if ($project->credits!=='')
        
    <section class="w-full pt-20">
        {!! $project->credits !!}
    </section>
    @endif
    <section class="grid w-full grid-cols-2 pt-20 h-50">
        <div class="relative col-span-1 overflow-hidden">
            @php
                $identficador = ($project->id);
                $anterior = App\Models\ProjectModel::where('id', '<',$identficador)->where('isedit',0)->orderBy('id', 'desc')->first();

            @endphp
            @if ($anterior!=null)
            @php
            $images = json_decode($anterior->images, true);
            @endphp
            <a
        href="{{ route('project', ['id' => Str::replace(' ', '_', Str::lower($anterior->title))]) }}" >
            <img data-src="{{ asset('assets/projects/' . Str::replace(' ', '_', Str::lower($anterior->title)) . '/' . $images[$anterior->portada===0?1:$anterior->portada]) }}"
            alt="" class="object-cover object-center w-full h-full lazy">
            <div class="absolute bottom-0 left-0 hidden w-full h-full transition-all duration-300 bg-black bg-opacity-0 lg:block hover:bg-opacity-50 project-hover">
                <h2 class="absolute text-3xl text-gray-100 transition-all duration-150 delay-75 title left-12">{{ $anterior->title }}</h2>
            </div>
            <div class="absolute bottom-0 left-0 block w-full h-full transition-all duration-300 bg-black bg-opacity-50 lg:hidden">
                <h2 class="absolute text-gray-100 transition-all duration-150 delay-75 transform -translate-x-1/2 translate-y-1/2 bottom-1/2 left-1/2">{{ $anterior->title }}</h2>
            </div>
        </a>
            @endif
        </div>
        <div class="relative col-span-1 overflow-hidden">
            @php
            $identficador = ($project->id);
            $siguiente = App\Models\ProjectModel::where('id', '>',$identficador)->where('isedit',0)->first();

        @endphp
        @if ($siguiente!=null)
        @php
        $images = json_decode($siguiente->images, true);
        @endphp
        <a
    href="{{ route('project', ['id' => Str::replace(' ', '_', Str::lower($siguiente->title))]) }}" >
        <img data-src="{{ asset('assets/projects/' . Str::replace(' ', '_', Str::lower($siguiente->title)) . '/' . $images[$siguiente->portada===0?1:$siguiente->portada]) }}"
        alt="" class="object-cover object-center w-full h-full lazy">
        <div class="absolute bottom-0 left-0 hidden w-full h-full transition-all duration-300 bg-black bg-opacity-0 lg:block hover:bg-opacity-50 project-hover">
            <h2 class="absolute text-3xl text-gray-100 transition-all duration-150 delay-75 title left-12">{{ $siguiente->title }}</h2>
        </div>
        <div class="absolute bottom-0 left-0 block w-full h-full transition-all duration-300 bg-black bg-opacity-50 lg:hidden">
            <h2 class="absolute text-gray-100 transition-all duration-150 delay-75 transform -translate-x-1/2 translate-y-1/2 bottom-1/2 left-1/2">{{ $siguiente->title }}</h2>
        </div>
    </a>
        @endif
        </div>

    </section>
    
@endsection
@push('script')
    <script>
        window.addEventListener('load', function () {

            document.querySelectorAll('.video').forEach(function (video) {

                video.querySelector('iframe').classList.add('w-full');
                video.querySelector('iframe').classList.add('h-full');
                video.querySelector('iframe').removeAttribute('height');
                video.querySelector('iframe').removeAttribute('width');
            });
        });
        </script>
@endpush
