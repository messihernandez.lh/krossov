/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
import VueAxios from "vue-axios";
import axios from "axios";
import route from 'ziggy-js';
import vueVimeoPlayer from 'vue-vimeo-player'
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/all";
import Hammer from "hammerjs";

// const response = await fetch('/api/ziggy');
window.Ziggy = '';
window.Route = route;

/**
 * The following block of code may bxe used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('v-zoombox', require('./components/vzoom.vue').default);
Vue.component('v-vimeo', require('./components/vimeoplayer.vue').default);
Vue.component('v-banner', require('./components/bannerOne.vue').default);
//Vue.component('v-projects', require('./components/vprojects.vue').default);
Vue.use(VueAxios, axios);
Vue.use(vueVimeoPlayer)
window.isEnableMenu = false;
window.hammerTime = null;
window.hammerTimeTwo = null;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.initVue = () => {
    console.log('init vue');
    window.appVue = new Vue({
        el: '#app',
        created() {
            axios.get('/api/ziggy').then((response) => {
                window.Ziggy = response.data;
            });
        },
        mounted() {
            if (window.hammerTime == null) {
                window.hammerTime = new Hammer(document.querySelector("#barra-menu"));
                // window.hammerTime.on("swipeleft swiperight swipeup swipedown",(event)=>{
                window.hammerTime.on("swipeleft", (event) => {

                    if (event.type == "swipeleft" && window.isEnableMenu === false) {
                        document.querySelector(".sidebar").classList.add("active");
                        window.isEnableMenu = true;
                        document.querySelector('.close-menu').classList.remove('hidden');
                        document.body.classList.add("overflow-hidden");
                    }
                    // if(event.type=="swiperight" && window.isEnableMenu===true){
                    //     document.querySelector(".sidebar").classList.remove("active");
                    //     window.isEnableMenu=false;
                    //     document.querySelector('.close-menu').classList.add('hidden');
                    //     document.body.classList.remove("overflow-hidden");
                    // }
                    // console.log(event.type);
                });
                // console.log("mounted");
            }
            if (window.hammerTimeTwo == null) {
                window.hammerTime = new Hammer(document.body);
                window.hammerTime.on("swiperight", (event) => {
                    if (event.type == "swiperight" && window.isEnableMenu === true) {
                        document.querySelector(".sidebar").classList.remove("active");
                        window.isEnableMenu = false;
                        document.querySelector('.close-menu').classList.add('hidden');
                        document.body.classList.remove("overflow-hidden");
                    }
                    // console.log(event.type);
                });
                // console.log("mounted");
            }
        },
    });
};
window.initVue();